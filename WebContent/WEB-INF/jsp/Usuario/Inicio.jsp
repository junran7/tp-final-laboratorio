<%@ page language="java" contentType="text/html;charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*, frgp.utn.edu.ar.dominio.*"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

<!-- Title -->
<title>IGDb - Inicio</title>

<!-- Favicon -->
<link rel="icon" href="images/core-img/favicon.ico">

<!-- Stylesheet -->
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet"
	href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css">

</head>

<body>
	<!-- Preloader -->
	<div class="preloader d-flex align-items-center justify-content-center">
		<div class="spinner">
			<div class="bounce1"></div>
			<div class="bounce2"></div>
			<div class="bounce3"></div>
		</div>
	</div>

	<!-- ##### Header Area Start ##### -->
	<header class="header-area wow fadeInDown" data-wow-delay="500ms">
	<!-- Top Header Area -->
	<div class="top-header-area">
		<div class="container h-100">
			<div class="row h-100 align-items-center">
				<div
					class="col-12 d-flex align-items-center justify-content-between">
					<!-- Logo Area -->
					<div class="logo">
						<a href="Inicio.html"><img src="images/core-img/logo.png"
							alt=""></a>
					</div>

					<!-- Search & Login Area -->
					<div class="search-login-area d-flex align-items-center">
						<!-- Top Search Area -->
						<div class="top-search-area">
							<form action="BuscarSimple.html" method="post">
								<input type="search" name="topSearch" id="topSearch"
									placeholder="Buscar" value='<%= (String)request.getAttribute("topSearch") != null ? (String)request.getAttribute("topSearch") : ""%>'>
								<button type="submit" class="btn">
									<i class="fa fa-search"></i>
								</button>
							</form>
						</div>
						<!-- Login Area -->
						<%
                            if(session.getAttribute("User")!=null && ((Usuario)session.getAttribute("User")).getUsuarioNombre()!=null)
                        	{
                        		
                        		%>
						<div class="login-area">
							<a href="MisDatos.html"><span><%= ((Usuario)session.getAttribute("User")).getUsuarioNombre() %></span></a>
						</div>

						<div class="login-area">
							<a href="NuevaPeticion.html"><span>Pedir juego
									faltante</span></a>
						</div>

						<form method="post" action="Desconectar.html">
							<div class="login-area">
								<input type="submit" name="btnCerrarSesion" value="Desconectar"
									style="border-style: none" />
							</div>
						</form>

						<%
                        	}
                        	else
                        	{
                        		%>
						<div class="login-area">
							<a href="Login.html"><span>Acceder / Registro</span> <i
								class="fa fa-lock" aria-hidden="true"></i></a>
						</div>
						<%
                        	}
                            %>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Navbar Area -->
	<div class="egames-main-menu" id="sticker">
		<div class="classy-nav-container breakpoint-off">
			<div class="container">
				<!-- Menu -->
				<nav class="classy-navbar justify-content-between" id="egamesNav">

				<!-- Navbar Toggler -->
				<div class="classy-navbar-toggler">
					<span class="navbarToggler"><span></span><span></span><span></span></span>
				</div>

				<!-- Menu -->
				<div class="classy-menu">

					<!-- Close Button -->
					<div class="classycloseIcon">
						<div class="cross-wrap">
							<span class="top"></span><span class="bottom"></span>
						</div>
					</div>

					<!-- Nav Start -->
					<div class="classynav">
						<ul>
							<li><a href="game-review.html">Juegos</a></li>
							<li><a href="Buscar.html">Búsqueda avanzada</a></li>
							<%
								if(session.getAttribute("User")!=null)
	                            {
	                            	if( ((Usuario)session.getAttribute("User")).getUsuarioNombre() != null && ((Usuario)session.getAttribute("User")).getUsuarioAdmin())
	                            	{
	                            	%>
	                            		<li><a href="ListarUsuariosAdmin.html">Listar usuarios</a></li>
										<li><a href="ListadoPeticiones.html">Listar peticiones</a></li>
										<li><a href="ABMLDesarrolladores.html">Desarrolladores</a></li>
										<li><a href="ABMLDistribuidores.html">Distribuidores</a></li>
										<li><a href="ABMLGeneros.html">Géneros</a></li>
										<li><a href="ABMLPaises.html">Países</a></li>
										<li><a href="ABMLPlataformas.html">Plataformas</a></li>
										<li><a href="AgregarJuego.html">Agregar Juego</a></li>
	                            	<%
	                            			}
	                            		}
                                    %>
						</ul>
					</div>
					<!-- Nav End -->
				</div>
				</nav>
			</div>
		</div>
	</div>
	</header>
	<!-- ##### Header Area End ##### -->
	<%
		List<Juego> listaTop = null;
		//Obtiene los productos del controlador (Servlet)
		if(request.getAttribute("JuegosTop")!=null){
			listaTop =  (List<Juego>) (request.getAttribute("JuegosTop"));
		}
		List<Juego> listaNew = null;
		//Obtiene los productos del controlador (Servlet)
		if(request.getAttribute("JuegosNew")!=null){
			listaNew =  (List<Juego>) (request.getAttribute("JuegosNew"));
		}
		System.out.println(listaTop);
	%>
	<!-- ##### Monthly Picks Area Start ##### -->
	<section class="monthly-picks-area section-padding-100 bg-pattern">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="left-right-pattern"></div>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-12">
				<!-- Title -->
				<h2 class="section-title mb-70 wow fadeInUp" data-wow-delay="100ms">Catálogo</h2>
			</div>
		</div>

		<div class="row">
			<div class="col-12">
				<ul class="nav nav-tabs wow fadeInUp" data-wow-delay="300ms"
					id="myTab" role="tablist">
					<li class="nav-item"><a class="nav-link active"
						id="popular-tab" data-toggle="tab" href="#popular" role="tab"
						aria-controls="popular" aria-selected="true">Más populares</a></li>
					<li class="nav-item"><a class="nav-link" id="latest-tab"
						data-toggle="tab" href="#latest" role="tab" aria-controls="latest"
						aria-selected="false">Más recientes</a></li>
				</ul>
			</div>
		</div>
	</div>

	<div class="tab-content wow fadeInUp" data-wow-delay="500ms"
		id="myTabContent">
		<div class="tab-pane fade show active" id="popular" role="tabpanel"
			aria-labelledby="popular-tab">
			<!-- Popular Games Slideshow -->
			<div class="popular-games-slideshow owl-carousel">
				<%
					if(listaTop != null && listaTop.size() > 0){
						for(int i=0; i< listaTop.size(); i++) {
						%>
							<form class="single-games-slide" action="mostrarJuego.html" method="post">
							<input type="hidden" name="idGame" value="<%=listaTop.get(i).getJuegoId()%>">
								<input type="image" name="verJuego" alt="submit" src="<%="images/juegosTapas/"%><%=listaTop.get(i).getJuegoId()%><%=".jpg"%>" style="margin-right: 20px;max-height: 300px">
								<div class="slide-text">
									<a href="#" class="game-title"><%=listaTop.get(i).getJuegoNombre()%></a>
									<div class="meta-data">
										<a><%=listaTop.get(i).getJuegoPromedioResenas()%> / 10</a>
									</div>
								</div>
							</form>
						<%
						if(i == 9)
							i = listaTop.size();
						}
					}
				%>
			</div>
		</div>
		<div class="tab-pane fade" id="latest" role="tabpanel"
			aria-labelledby="latest-tab">
			<!-- Latest Games Slideshow -->
			<div class="latest-games-slideshow owl-carousel">

				<%
					if(listaNew != null && listaNew.size() > 0){
						for(int i=0; i< listaNew.size(); i++) {
						%>
							<form class="single-games-slide" action="mostrarJuego.html" method="post">
							<input type="hidden" name="idGame" value="<%=listaNew.get(i).getJuegoId()%>">
								<input type="image" name="verJuego" alt="submit" src="<%="images/juegosTapas/"%><%=listaNew.get(i).getJuegoId()%><%=".jpg"%>" style="max-height: 300px">
								<div class="slide-text">
									<a href="#" class="game-title"><%=listaNew.get(i).getJuegoNombre()%></a>
									<div class="meta-data">
										<a><%=listaNew.get(i).getJuegoPromedioResenas()%> / 10</a>
									</div>
								</div>
							</form>
						<%
						if(i == 9)
							i = listaNew.size();
						}
					}
				%>

			</div>
		</div>
	</div>
	</section>
	<!-- ##### Monthly Picks Area End ##### -->

	<!-- ##### Games Area Start ##### -->
	<div class="games-area section-padding-100-0">
		<div class="container">
			<div class="row">
				<!-- Single Games Area -->
				<form class="col-12 col-md-4" action="BuscarParametros.html" method="post">
					<div class="single-games-area text-center mb-100 wow fadeInUp"
						data-wow-delay="100ms">
						<input type="hidden" value="5" name="plat">
						<img src="images/consoles/ps4.png" alt=""> <input type="submit" name="buscarPlat" value="Ver Juegos"
							class="btn egames-btn mt-30" style="width:90%">
					</div>
				</form>

				<!-- Single Games Area -->
				<form class="col-12 col-md-4" action="BuscarParametros.html" method="post">
					<div class="single-games-area text-center mb-100 wow fadeInUp"
						data-wow-delay="300ms">
						<input type="hidden" value="13" name="plat">
						<img src="images/consoles/nswitch.png" alt=""> <input type="submit" name="buscarPlat" value="Ver Juegos"
							class="btn egames-btn mt-30" style="width:90%">
					</div>
				</form>

				<!-- Single Games Area -->
				<form class="col-12 col-md-4" action="BuscarParametros.html" method="post">
					<div class="single-games-area text-center mb-100 wow fadeInUp"
						data-wow-delay="500ms">
						<input type="hidden" value="8" name="plat">
						<img src="images/consoles/xone.png" alt=""> <input type="submit" name="buscarPlat" value="Ver Juegos"
							class="btn egames-btn mt-30" style="width:90%">
					</div>
				</form>
			</div>
			<div class="row">
				<!-- Single Games Area -->
				<form class="col-12 col-md-4" action="BuscarParametros.html" method="post">
					<div class="single-games-area text-center mb-100 wow fadeInUp"
						data-wow-delay="100ms">
						<input type="hidden" value="4" name="plat">
						<img src="images/consoles/ps3.png" alt=""> <input type="submit" name="buscarPlat" value="Ver Juegos"
							class="btn egames-btn mt-30" style="width:90%">
					</div>
				</form>

				<!-- Single Games Area -->
				<form class="col-12 col-md-4" action="BuscarParametros.html" method="post">
					<div class="single-games-area text-center mb-100 wow fadeInUp"
						data-wow-delay="300ms">
						<input type="hidden" value="15" name="plat">
						<img src="images/consoles/win10.png" alt=""> <input type="submit" name="buscarPlat" value="Ver Juegos"
							class="btn egames-btn mt-30" style="width:90%">
					</div>
				</form>

				<!-- Single Games Area -->
				<form class="col-12 col-md-4" action="BuscarParametros.html" method="post">
					<div class="single-games-area text-center mb-100 wow fadeInUp"
						data-wow-delay="500ms">
						<input type="hidden" value="7" name="plat">
						<img src="images/consoles/x360.png" alt=""> <input type="submit" name="buscarPlat" value="Ver Juegos"
							class="btn egames-btn mt-30" style="width:90%">
					</div>
				</form>
			</div>
			<div class="row">
				<!-- Single Games Area -->
				<form class="col-12 col-md-4" action="BuscarParametros.html" method="post">
					<div class="single-games-area text-center mb-100 wow fadeInUp"
						data-wow-delay="100ms">
						<input type="hidden" value="11" name="plat">
						<img src="images/consoles/nwii.png" alt=""> <input type="submit" name="buscarPlat" value="Ver Juegos"
							class="btn egames-btn mt-30" style="width:90%">
					</div>
				</form>

				<!-- Single Games Area -->
				<form class="col-12 col-md-4" action="BuscarParametros.html" method="post">
					<div class="single-games-area text-center mb-100 wow fadeInUp"
						data-wow-delay="300ms">
						<input type="hidden" value="19" name="plat">
						<img src="images/consoles/iosandroid.png" alt=""> <input type="submit" name="buscarPlat"
							class="btn egames-btn mt-30" style="width:90%">
					</div>
				</form>

				<!-- Single Games Area -->
				<form class="col-12 col-md-4" action="BuscarParametros.html" method="post">
					<div class="single-games-area text-center mb-100 wow fadeInUp"
						data-wow-delay="500ms">
						<input type="hidden" value="5" name="plat">
						<img src="images/consoles/n3ds.png" alt=""> <input type="submit" name="buscarPlat"
							class="btn egames-btn mt-30" style="width:90%">
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- ##### Games Area End ##### -->

	<!-- ##### Footer Area Start ##### -->
	<footer class="footer-area"> <!-- Copywrite Area -->
	<div class="copywrite-content">
		<div class="container h-100">
			<div class="row h-100 align-items-center">
				<div class="col-12 col-sm-5">
					<!-- Copywrite Text -->
					<p class="copywrite-text">
						<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
						Copyright &copy;
						<script>document.write(new Date().getFullYear());</script>
						All rights reserved |
					</p>
				</div>
				<div class="col-12 col-sm-7">
					<!-- Footer Nav -->
					<div class="footer-nav">
						<ul>
							<li><a href="index.html">IGDb</a></li>
							<li><a href="game-review.html">Juegos</a></li>
							<li><a href="post.html">Búsqueda avanzada</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	</footer>
	<!-- ##### Footer Area End ##### -->

	<!-- ##### All Javascript Script ##### -->
	<!-- jQuery-2.2.4 js -->
	<script src="js/jquery/jquery-2.2.4.min.js"></script>
	<!-- Popper js -->
	<script src="js/bootstrap/popper.min.js"></script>
	<!-- Bootstrap js -->
	<script src="js/bootstrap/bootstrap.min.js"></script>
	<!-- All Plugins js -->
	<script src="js/plugins/plugins.js"></script>
	<!-- Active js -->
	<script src="js/active.js"></script>
</body>

</html>