<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
       <%@page import="javax.websocket.Session"%>
<%@ page import="java.util.*, frgp.utn.edu.ar.dominio.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>IGDb - Nueva Petición</title>
	<meta charset="UTF-8">

	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css">
<!--===============================================================================================-->
</head>
<body>
		<%
			Usuario user = new Usuario();
			if(session.getAttribute("User")==null)
				response.sendRedirect("Registro.html");
			else
				user = (Usuario)session.getAttribute("User");
		%>
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<form method="post" action="Peticion.html" class="login100-form validate-form">
					<span class="login100-form-title p-b-34">
						Petición Nueva
					</span>
					<%
							if(request.getAttribute("respuesta")!=null){
								%>
								<span class="login100-form-title p-b-34">
								<%= request.getAttribute("respuesta") %>
								</span>
								
								<%
							}
					%>
					<div class="wrap-input100 rs1-wrap-input100 validate-input m-b-20" data-validate="Debe escribir un nombre" style="width:100%">
						<input id="first-name" class="input100" type="text" name="peticionNombre" placeholder="Nombre del juego" style="width: 100%">
						<span class="focus-input100"></span>
					</div>
					
					<div class="wrap-input100 login100-form-title m-b-20">
						<textarea class="input100" style="margin-bottom:10px; resize:none; height:80px" name="peticionDetalles" placeholder="Detalles adicionales."></textarea>
					</div>
					
					<div class="container-login100-form-btn">
						<input type="submit" class="login100-form-btn" value="Enviar Petición" name="btnEnviarPeticion" style="margin-bottom: 20px"/>
					</div>
										
					<div class="w-full text-center p-t-27">
						<a href="Inicio.html" class="txt1">
							Volver al Inicio
						</a>
					</div>

				</form>
				<div class="login100-more" style="background-image: url('images/bg-01.jpg');"></div>
			</div>
		</div>
	</div>
	
	

	<div id="dropDownSelect1"></div>
	
<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
	<script>
		$(".selection-2").select2({
			minimumResultsForSearch: 20,
			dropdownParent: $('#dropDownSelect1')
		});
	</script>
<!--===============================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>