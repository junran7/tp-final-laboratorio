<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*, frgp.utn.edu.ar.dominio.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="es">

<head>
<meta charset="UTF-8">
<meta name="description" content="">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

<!-- Title -->
<title>IGDb - Reseña</title>

<!-- Favicon -->
<link rel="icon" href="images/core-img/favicon.ico">

<!-- Stylesheet -->
<link rel="stylesheet" href="style.css">
<link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css">
</head>

<body>
	<!-- Preloader -->
	<div class="preloader d-flex align-items-center justify-content-center">
		<div class="spinner">
			<div class="bounce1"></div>
			<div class="bounce2"></div>
			<div class="bounce3"></div>
		</div>
	</div>

	<!-- ##### Header Area Start ##### -->
	<header class="header-area wow fadeInDown" data-wow-delay="500ms">
	<!-- Top Header Area -->
	<div class="top-header-area">
		<div class="container h-100">
			<div class="row h-100 align-items-center">
				<div
					class="col-12 d-flex align-items-center justify-content-between">
					<!-- Logo Area -->
					<div class="logo">
						<a href="Inicio.html"><img src="images/core-img/logo.png"
							alt=""></a>
					</div>

					<!-- Search & Login Area -->
					<div class="search-login-area d-flex align-items-center">
						<!-- Top Search Area -->
						<div class="top-search-area">
							<form action="#" method="post">
								<input type="search" name="top-search" id="topSearch"
									placeholder="Buscar">
								<button type="submit" class="btn">
									<i class="fa fa-search"></i>
								</button>
							</form>
						</div>
						<!-- Login Area -->
						<%
                            if(session.getAttribute("User")!=null && ((Usuario)session.getAttribute("User")).getUsuarioNombre()!=null)
                        	{
                        		
                        		%>
						<div class="login-area">
							<a href="MisDatos.html"><span><%= ((Usuario)session.getAttribute("User")).getUsuarioNombre() %></span></a>
						</div>

						<div class="login-area">
							<a href="NuevaPeticion.html"><span>Pedir juego
									faltante</span></a>
						</div>

						<form method="post" action="Desconectar.html">
							<div class="login-area">
								<input type="submit" name="btnCerrarSesion" value="Desconectar"
									style="border-style: none" />
							</div>
						</form>

						<%
                        	}
                        	else
                        	{
                        		%>
						<div class="login-area">
							<a href="Login.html"><span>Acceder / Registro</span> <i
								class="fa fa-lock" aria-hidden="true"></i></a>
						</div>
						<%
                        	}
                            %>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Navbar Area -->
	<div class="egames-main-menu" id="sticker">
		<div class="classy-nav-container breakpoint-off">
			<div class="container">
				<!-- Menu -->
				<nav class="classy-navbar justify-content-between" id="egamesNav">

				<!-- Navbar Toggler -->
				<div class="classy-navbar-toggler">
					<span class="navbarToggler"><span></span><span></span><span></span></span>
				</div>

				<!-- Menu -->
				<div class="classy-menu">

					<!-- Close Button -->
					<div class="classycloseIcon">
						<div class="cross-wrap">
							<span class="top"></span><span class="bottom"></span>
						</div>
					</div>

					<!-- Nav Start -->
					<div class="classynav">
						<ul>
							<li><a href="game-review.html">Juegos</a></li>
							<li><a href="Buscar.html">Búsqueda avanzada</a></li>
							<%
								if(session.getAttribute("User")!=null)
	                            {
	                            	if( ((Usuario)session.getAttribute("User")).getUsuarioNombre() != null && ((Usuario)session.getAttribute("User")).getUsuarioAdmin())
	                            	{
	                            	%>
	                            		<li><a href="ListarUsuariosAdmin.html">Listar usuarios</a></li>
										<li><a href="ListadoPeticiones.html">Listar peticiones</a></li>
										<li><a href="ABMLDesarrolladores.html">Desarrolladores</a></li>
										<li><a href="ABMLDistribuidores.html">Distribuidores</a></li>
										<li><a href="ABMLGeneros.html">Géneros</a></li>
										<li><a href="ABMLPaises.html">Países</a></li>
										<li><a href="ABMLPlataformas.html">Plataformas</a></li>
										<li><a href="AgregarJuego.html">Agregar Juego</a></li>
	                            	<%
	                            			}
	                            		}
                                    %>
						</ul>
					</div>
					<!-- Nav End -->
				</div>
				</nav>
			</div>
		</div>
	</div>
	</header>
	<!-- ##### Header Area End ##### -->
	<%
		Juego juegoAMostrar = null;
	
		if(request.getAttribute("JuegoAMostrar")!=null){
			juegoAMostrar = (Juego) request.getAttribute("JuegoAMostrar");
		}
		String directorio = null;
		if(juegoAMostrar != null){
			directorio = "images/juegosTapas/" + juegoAMostrar.getJuegoId() + ".jpg";
		}
		Usuario user = new Usuario();
		if(session.getAttribute("User")==null)
			response.sendRedirect("Inicio.html");
		else
			user = (Usuario)session.getAttribute("User");
		
		Resena resena = null;
		if(request.getAttribute("ResenaAMostrar")!=null){
			resena = (Resena)request.getAttribute("ResenaAMostrar");
		}
	%>
	<!-- ##### Breadcrumb Area Start ##### -->
	<form action="AgregarResena.html" method="post">
	
		<div class="breadcrumb-area bg-img bg-overlay"
			style="background-image: url(images/background.jpg);">
			<div class="container h-100">
				<div class="row h-100 align-items-center">
					<!-- Breadcrumb Text -->
					<div class="col-12" style="max-width: 50%; text-align: center">
						<div class="breadcrumb-text">
						<input type="hidden" name="idGame" value="<%=juegoAMostrar.getJuegoId()%>">
						<input type="hidden" name="idUser" value="<%=user.getUsuarioId()%>">
						<input type="hidden" name="idResena" value="<%=resena != null ? resena.getResenaId() : ""%>">
							<h2><%= juegoAMostrar != null ? juegoAMostrar.getJuegoNombre() : "ERROR"%></h2>
						</div>
<!-- 						<a href="Juego.html"> -->
						<input type="submit" value="<%= resena != null ? "Modificar" : "Publicar" %>" name="publicarResena"
							style="display: inline; cursor: pointer; width: 180px; height: 42px; background-color: #20d8da; text-align: center; color: #fff; line-height: 42px; font-size: 14px; margin-top: 10px; margin-left: 10px; margin-right: 10px">
<!-- 						</a> -->
<!-- 						<a href="Juego.html"> -->
						<input type="submit" value="Cancelar" name="cancelarResena"
							style="display: inline; cursor: pointer; width: 180px; height: 42px; background-color: #20d8da; text-align: center; color: #fff; line-height: 42px; font-size: 14px; margin-top: 10px; margin-left: 10px; margin-right: 10px">
<!-- 						</a> -->
						<br><br>
						<div class="slidecontainer" style="align-self: center;">
							<div class="single-game-review-area style-2 mt-25">
								<div class="game-content">
									<a href="single-game-review.html" class="game-title" style="color: #ffffff">Calificación</a>
								</div>
							</div>
							<input type="range" min="1" max="10" value="<%= resena != null ? resena.getResenaPuntaje() : 5 %>" class="slider"
								id="calificacion" name="calif">
							<div class="single-game-review-area style-2 mt-10">
								<div class="game-content">
								<label class="game-title" id="calif" style="color:#ffffff"><%= resena != null ? resena.getResenaPuntaje() : 5 %>/10</label>
								</div>
							</div>
						</div>
					</div>
	
					<div style="width: 50%; height: 100%; text-align: center">
						<img
							style="max-height: 100%; max-width: 100%; vertical-align: center"
							src="<%=directorio%>" alt="">
					</div>
				</div>
			</div>
		</div>
		<!-- ##### Breadcrumb Area End ##### -->
		
		<!-- ##### Single Game Review Area Start ##### -->
		<section class="single-game-review-area section-padding-1">
		<div class="container">
	
	
			<div class="row align-items-center">
				<!-- *** Review Area *** -->
				<div class="col-12 col-md-6" style="flex: 0 0 100%; max-width: 100%">
					<div class="single-game-review-area style-2 mt-20">
						<div class="game-content">
							<a class="game-title">Reseña:</a>
							<textarea class="input100"
								style="margin-bottom: 10px; resize: none; height: 200px; width: 100%"
								name="textResena" placeholder="It's Perfect"><%= resena != null ? resena.getResenaDescripcion() : ""%></textarea>
						</div>
					</div>
				</div>
	
			</div>
		</div>
		</section>
	
	</form>
	<!-- ##### Single Game Review Area End ##### -->

	<!-- ##### Footer Area Start ##### -->
	<footer class="footer-area"> <!-- Copywrite Area -->
	<div class="copywrite-content">
		<div class="container h-100">
			<div class="row h-100 align-items-center">
				<div class="col-12 col-sm-5">
					<!-- Copywrite Text -->
					<p class="copywrite-text">
						<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
						Copyright &copy;
						<script>
							document.write(new Date().getFullYear());
						</script>
						All rights reserved |
					</p>
				</div>
				<div class="col-12 col-sm-7">
					<!-- Footer Nav -->
					<div class="footer-nav">
						<ul>
							<li><a href="index.html">IGDb</a></li>
							<li><a href="game-review.html">Juegos</a></li>
							<li><a href="post.html">Búsqueda Avanzada</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	</footer>
	<!-- ##### Footer Area End ##### -->

	<!-- ##### All Javascript Script ##### -->
	<!-- jQuery-2.2.4 js -->
	<script src="js/jquery/jquery-2.2.4.min.js"></script>
	<!-- Popper js -->
	<script src="js/bootstrap/popper.min.js"></script>
	<!-- Bootstrap js -->
	<script src="js/bootstrap/bootstrap.min.js"></script>
	<!-- All Plugins js -->
	<script src="js/plugins/plugins.js"></script>
	<!-- Active js -->
	<script src="js/active.js"></script>
	
	<script>
		$( document ).ready(function() {
		    $("#calificacion").on("change", function() {
		    	$("#calif").html($("#calificacion").val() + "/10");
		    });
		});
	</script>
</body>

</html>