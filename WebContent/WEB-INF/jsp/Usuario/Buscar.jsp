<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*, frgp.utn.edu.ar.dominio.*"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta charset="UTF-8">
<meta name="description" content="">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

<!-- Title -->
<title>IGDb - Búsqueda</title>

<!-- Favicon -->
<link rel="icon" href="images/core-img/favicon.ico">

<!-- Stylesheet -->
<link rel="stylesheet" href="style.css">
<link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css">
</head>

<body>
	<!-- Preloader -->
	<div class="preloader d-flex align-items-center justify-content-center">
		<div class="spinner">
			<div class="bounce1"></div>
			<div class="bounce2"></div>
			<div class="bounce3"></div>
		</div>
	</div>

	<!-- ##### Header Area Start ##### -->
	<header class="header-area wow fadeInDown" data-wow-delay="500ms">
	<!-- Top Header Area -->
	<div class="top-header-area">
		<div class="container h-100">
			<div class="row h-100 align-items-center">
				<div
					class="col-12 d-flex align-items-center justify-content-between">
					<!-- Logo Area -->
					<div class="logo">
						<a href="Inicio.html"><img src="images/core-img/logo.png"
							alt=""></a>
					</div>

					<!-- Search & Login Area -->
					<div class="search-login-area d-flex align-items-center">
						<!-- Top Search Area -->
						<div class="top-search-area">
							<form action="#" method="post">
								<input type="search" name="topSearch" id="topSearch"
									placeholder="Buscar" value='<%= (String)request.getAttribute("topSearch") != null ? (String)request.getAttribute("topSearch") : ""%>'>
								<button type="submit" class="btn">
									<i class="fa fa-search"></i>
								</button>
							</form>
						</div>
						<!-- Login Area -->
						<%
                            	if(session.getAttribute("User")!=null && ((Usuario)session.getAttribute("User")).getUsuarioNombre()!=null)
                            	{
                            		
                            		%>
						<div class="login-area">
		                                <a href="MisDatos.html"><span><%= ((Usuario)session.getAttribute("User")).getUsuarioNombre() %></span></a>
		                            </div>
		                            
		                            <div class="login-area">
		                                <a href="NuevaPeticion.html"><span>Pedir juego faltante</span></a>
		                            </div>
		                            
		                            <form method="post" action="ServletUsuarios">
		                            	<div class="login-area">
			                               <input type="submit" name="btnCerrarSesion" value="Desconectar" style="border-style: none"/>
			                            </div>
		                            </form>

						<%
                            	}
                            	else
                            	{
                            		%>
						<div class="login-area">
							<a href="Login.html"><span>Acceder / Registro</span> <i
								class="fa fa-lock" aria-hidden="true"></i></a>
						</div>
						<%
                            	}
                            %>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Navbar Area -->
	<div class="egames-main-menu" id="sticker">
		<div class="classy-nav-container breakpoint-off">
			<div class="container">
				<!-- Menu -->
				<nav class="classy-navbar justify-content-between" id="egamesNav">

				<!-- Navbar Toggler -->
				<div class="classy-navbar-toggler">
					<span class="navbarToggler"><span></span><span></span><span></span></span>
				</div>

				<!-- Menu -->
				<div class="classy-menu">

					<!-- Close Button -->
					<div class="classycloseIcon">
						<div class="cross-wrap">
							<span class="top"></span><span class="bottom"></span>
						</div>
					</div>

					<!-- Nav Start -->
					<div class="classynav">
						<ul>
							<li><a href="game-review.html">Juegos</a></li>
							<li><a href="Buscar.html">Búsqueda avanzada</a></li>
							<%
								if(session.getAttribute("User")!=null)
	                            {
	                            	if( ((Usuario)session.getAttribute("User")).getUsuarioNombre() != null && ((Usuario)session.getAttribute("User")).getUsuarioAdmin())
	                            	{
	                            	%>
	                            		<li><a href="ListarUsuariosAdmin.html">Listar usuarios</a></li>
										<li><a href="ListadoPeticiones.html">Listar peticiones</a></li>
										<li><a href="ABMLDesarrolladores.html">Desarrolladores</a></li>
										<li><a href="ABMLDistribuidores.html">Distribuidores</a></li>
										<li><a href="ABMLGeneros.html">Géneros</a></li>
										<li><a href="ABMLPaises.html">Países</a></li>
										<li><a href="ABMLPlataformas.html">Plataformas</a></li>
										<li><a href="AgregarJuego.html">Agregar Juego</a></li>
	                            	<%
	                            			}
	                            		}
                                    %>
						</ul>
					</div>
					<!-- Nav End -->
				</div>
				</nav>
			</div>
		</div>
	</div>
	</header>
	<!-- ##### Header Area End ##### -->
	<%
		List<Desarrollador> listaDesarrolladores = null;
		List<Distribuidor> listaDistribuidores = null;
		List<Plataforma> listaPlataformas = null;
		List<Genero> listaGeneros = null;
		if(request.getAttribute("listaDesarrolladores")!=null){
			listaDesarrolladores =  (List<Desarrollador>) (request.getAttribute("listaDesarrolladores"));
		}
		if(request.getAttribute("listaDistribuidores")!=null){
			listaDistribuidores =  (List<Distribuidor>) (request.getAttribute("listaDistribuidores"));
		}
		if(request.getAttribute("listaPlataformas")!=null){
			listaPlataformas =  (List<Plataforma>) (request.getAttribute("listaPlataformas"));
		}
		if(request.getAttribute("listaGeneros")!=null){
			listaGeneros =  (List<Genero>) (request.getAttribute("listaGeneros"));
		}
		List<Juego> listaResultados = null;
		if(request.getAttribute("listaResultados")!=null){
			listaResultados = (List<Juego>) request.getAttribute("listaResultados");
		}
	%>
	<!-- ##### Breadcrumb Area Start ##### -->
	<section class="breadcrumb-area bg-img bg-overlay"
		style="background-image: url(images/background.jpg);">
	<div class="container h-100">
		<div class="row h-100">
			<!-- Breadcrumb Text -->
			<form class="col-12" style="text-align: center;" method="post" action="BuscarParametros.html">
				<div class="breadcrumb-text" style="margin-top: 10px">
					<h2>Búsqueda avanzada</h2>
				</div> <br>
				<input type="text" name="nombre" id="juegoNombre" placeholder=" Nombre" class="search-field" Style="width: 400px" value="<%= (String)request.getAttribute("topSearch") != null ? (String)request.getAttribute("topSearch") : ""%>">
				<input type="number" min="1" max="9999" name="fecha" id="juegoFecha" placeholder=" Año" class="search-field" Style="width: 50px">
				<br>
				<select name="genero" id="juegoGenero" placeholder=" Género" class="search-field" Style="width: 250px">
					<option value="322">Género</option>
					<%
						if(listaGeneros.size() > 0){
							%>
								<c:forEach items="${listaGeneros}" var="abr">
									<option value="${abr.getGeneroId()}">${abr.getGeneroNombre()}</option>
								</c:forEach>
							<%
						}
					%>
				</select>
				<select name="plat" id="juegoPlataforma" placeholder=" Plataforma" class="search-field" Style="width: 200px">
					<option value="322">Plataforma</option>
					<%
						if(listaPlataformas.size() > 0){
							%>
								<c:forEach items="${listaPlataformas}" var="abr">
									<option value="${abr.getPlataformaId()}">${abr.getPlataformaNombre()}</option>
								</c:forEach>
							<%
						}
					%>
				</select>
				<br>
				<select name="dev" id="juegoDesarrollador" placeholder=" Desarrollador" class="search-field" Style="width: 225px">
					<option value="322">Desarrollador</option>
					<%
						if(listaDesarrolladores.size() > 0){
							%>
								<c:forEach items="${listaDesarrolladores}" var="abr">
									<option value="${abr.getDesarrolladorId()}">${abr.getDesarrolladorNombre()}</option>
								</c:forEach>
							<%
						}
					%>
				</select>
				<select name="pub" id="juegoDistribuidor" placeholder=" Distribuidor" class="search-field" Style="width: 225px">
					<option value="322">Distribuidor</option>
					<%
						if(listaDistribuidores.size() > 0){
							%>
								<c:forEach items="${listaDistribuidores}" var="abr">
									<option value="${abr.getDistribuidorId()}">${abr.getDistribuidorNombre()}</option>
								</c:forEach>
							<%
						}
					%>
				</select>
				<br>
				<select name="region" id="juegoRegion" placeholder=" Región" class="search-field" Style="width: 250px">
					<option value="0">Región</option>
					<option value="1">US (Americas)</option>
					<option value="2">EU (Europa)</option>
					<option value="3">JP (Asia)</option>
				</select>
				<input type="number" min="0" max="10" name="minprom" id="juegoPromMin" placeholder=" Prom. mínimo" class="search-field" Style="width: 90px">
				<input type="number" min="0" max="10" name="maxprom" id="juegoPromMax" placeholder=" Prom. máximo" class="search-field" Style="width: 90px">
				
					<br>
				<input type="submit" name="buscarParametros" value="Buscar"
					style="display: inline; cursor: pointer; width: 180px; height: 42px; background-color: #20d8da; text-align: center; color: #fff; line-height: 42px; font-size: 14px; margin-top: 10px; margin-left: 10px; margin-right: 10px">
				<input type="submit" name="buscarLimpiar" value="Limpiar campos"
					style="display: inline; cursor: pointer; width: 180px; height: 42px; background-color: #20d8da; text-align: center; color: #fff; line-height: 42px; font-size: 14px; margin-top: 10px; margin-left: 10px; margin-right: 10px">
			</form>
		</div>
	</div>
	</section>
	<!-- ##### Breadcrumb Area End ##### -->

	<!-- ##### Articles Area Start ##### -->
	<section class="articles-area section-padding-0-100">
	<div class="container">
		<div class="row justify-content-center">
			<!-- Articles Post Area -->
			<div class="col-12">
				<div class="mt-100">
					<%
						if(listaResultados != null && listaResultados.size() > 0){
							%>
								<c:forEach items="${listaResultados}" var="abr">
									<form class="single-articles-area d-flex flex-wrap mb-30" action="mostrarJuego.html" method="post">
									<input type="hidden" name="idGame" value="${abr.getJuegoId()}">
										<div class="article-thumbnail" style="max-height: 300px; overflow: hidden;">
											<img src="<%="images/juegosTapas/"%>${abr.getJuegoId()}<%=".jpg"%>" alt="" >
										</div>
										<div class="article-content">
											<input type="submit" id="${abr.getJuegoId()}" name="verJuego" value="${abr.getJuegoNombre()}" class="post-title" style="color: white; margin-left:0px; width: 500px">
											<div class="post-meta">
												<a href="#" class="post-date" style="font-size: 18px">${abr.getJuegoPromedioResenas()} / 10 </a>
											</div>
											<p>${abr.getJuegoDescripcion()}</p>
										</div>
									</form>
								</c:forEach>
							<%
						} else if (listaResultados == null) {
					%>
							No se ha realizado ninguna búsqueda.
					<%
						} else {
					%>
							La búsqueda realizada no ha devuelto ningún resultado.
					<%
						}
					%>
					
				</div>
			</div>

		</div>
	</div>
	</section>
	<!-- ##### Articles Area End ##### -->


	<!-- ##### Footer Area Start ##### -->
	<footer class="footer-area"> <!-- Copywrite Area -->
	<div class="copywrite-content">
		<div class="container h-100">
			<div class="row h-100 align-items-center">
				<div class="col-12 col-sm-5">
					<!-- Copywrite Text -->
					<p class="copywrite-text">
						<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
						Copyright &copy;
						<script>
							document.write(new Date().getFullYear());
						</script>
						All rights reserved |
					</p>
				</div>
				<div class="col-12 col-sm-7">
					<!-- Footer Nav -->
					<div class="footer-nav">
						<ul>
							<li><a href="index.html">IGDb</a></li>
							<li><a href="game-review.html">Juegos</a></li>
							<li><a href="post.html">Búsqueda Avanzada</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	</footer>
	<!-- ##### Footer Area End ##### -->

	<!-- ##### All Javascript Script ##### -->
	<!-- jQuery-2.2.4 js -->
	<script src="js/jquery/jquery-2.2.4.min.js"></script>
	<!-- Popper js -->
	<script src="js/bootstrap/popper.min.js"></script>
	<!-- Bootstrap js -->
	<script src="js/bootstrap/bootstrap.min.js"></script>
	<!-- All Plugins js -->
	<script src="js/plugins/plugins.js"></script>
	<!-- Active js -->
	<script src="js/active.js"></script>
</body>

</html>