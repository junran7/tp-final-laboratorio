package frgp.utn.edu.ar.servicio;

import java.util.ArrayList;

import frgp.utn.edu.ar.dominio.Distribuidor;

public interface DistribuidorServicio {

	ArrayList<Distribuidor> obtenerDistribuidores();

	void insertarDistribuidor(Distribuidor distribuidor);

    void eliminarDistribuidor(Integer idDistribuidor);

	void actualizarDistribuidor(Distribuidor distribuidor);
	
	Distribuidor obtenerDistribuidor(Integer idDistribuidor);
	
}
