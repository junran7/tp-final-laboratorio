package frgp.utn.edu.ar.servicio;

import java.util.ArrayList;

import frgp.utn.edu.ar.dominio.Desarrollador;

public interface DesarrolladorServicio {
	
	ArrayList<Desarrollador> obtenerDesarrolladores();

	void insertarDesarrollador(Desarrollador desarrollador);

    void eliminarDesarrollador(Integer idDesarrollador);

	void actualizarDesarrollador(Desarrollador desarrollador);
	
	Desarrollador obtenerDesarrollador(Integer idDesarrollador);
	
}
