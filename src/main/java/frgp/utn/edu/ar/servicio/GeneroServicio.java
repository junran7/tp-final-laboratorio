package frgp.utn.edu.ar.servicio;

import java.util.ArrayList;

import frgp.utn.edu.ar.dominio.Genero;

public interface GeneroServicio {
	ArrayList<Genero> obtenerGeneros();

	void insertarGenero(Genero genero);

    void eliminarGenero(Integer idGenero);

	void actualizarGenero(Genero genero);

	Genero obtenerGenero(Integer id);
}
