package frgp.utn.edu.ar.servicio;

import java.util.ArrayList;

import frgp.utn.edu.ar.dominio.Pais;

public interface PaisServicio {
	
	ArrayList<Pais> obtenerPaises();

	void insertarPais(Pais pais);

    void eliminarPais(Integer idPais);

	void actualizarPais(Pais pais);

	Pais obtenerPais(Integer id);
}
