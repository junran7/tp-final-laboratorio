package frgp.utn.edu.ar.servicio;

import java.util.ArrayList;

import frgp.utn.edu.ar.dominio.Plataforma;

public interface PlataformaServicio {

	ArrayList<Plataforma> obtenerPlataformas();

	void insertarPlataforma(Plataforma plataforma);

    void eliminarPlataforma(Integer idPlataforma);

	void actualizarPlataforma(Plataforma plataforma);
	
	Plataforma obtenerPlataforma(Integer idPlataforma);
	
}
