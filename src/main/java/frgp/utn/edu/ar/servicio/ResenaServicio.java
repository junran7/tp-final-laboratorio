package frgp.utn.edu.ar.servicio;

import java.util.ArrayList;

import frgp.utn.edu.ar.dominio.Resena;

public interface ResenaServicio {
	
	ArrayList<Resena> obtenerResenas();

	void insertarResena(Resena resena);

    void eliminarResena(Integer idResena);

	void actualizarResena(Resena resena);

	Resena obtenerResena(Integer resenaId);

	String obtenerPromedioNuevo(Integer puntajeJuego, Integer idJuego);

	ArrayList<Resena> obtenerResenasPorJuego(Integer idJuego);

	ArrayList<Resena> obtenerResenasPorJuegoYUsuario(Integer idJuego, Integer idUsuario);
	
}
