package frgp.utn.edu.ar.servicio;

import java.util.ArrayList;

import frgp.utn.edu.ar.dominio.Usuario;


public interface UsuarioServicio {

	ArrayList<Usuario> obtenerUsuarios();

	Usuario obtenerUnRegistro(String nombreUser);

	void insertarUsuario(Usuario usuario);

    void eliminarUsuario(Integer idUser);
    
    void banearUsuario(Usuario user);
    
    void desbanearUsuario(Usuario user);
    
    void adminearUsuario(Usuario user);

	void actualizarUsuario(Usuario usuario);
	
	String registrarUsuario(Usuario user);

	boolean existeEmail(String email);
	
	boolean existeEmail(String email, int id);
	
	Usuario obtenerUsuarioLogin(String email, String pass);
	
	public boolean contrasenaValida(int id, String pass);
	
	public Usuario obtenerUsuario(int id);
}
