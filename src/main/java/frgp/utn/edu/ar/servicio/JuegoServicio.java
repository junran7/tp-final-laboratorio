package frgp.utn.edu.ar.servicio;

import java.util.ArrayList;

import frgp.utn.edu.ar.dominio.Juego;

public interface JuegoServicio {
	ArrayList<Juego> obtenerJuegos();

	Integer insertarJuego(Juego juego);

    void eliminarJuego(Integer idJuego);

	void actualizarJuego(Juego juego);

	Juego obtenerJuego(Integer idJuego);

	ArrayList<Juego> obtenerJuegosPorNombre(String juegoNombre);

	ArrayList<Juego> queryJuego(String query);
}
