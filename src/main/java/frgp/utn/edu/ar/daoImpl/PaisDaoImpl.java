package frgp.utn.edu.ar.daoImpl;

import java.util.ArrayList;

import org.hibernate.SessionFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import frgp.utn.edu.ar.dao.PaisDao;
import frgp.utn.edu.ar.dominio.Pais;

public class PaisDaoImpl implements PaisDao{


	private HibernateTemplate hibernateTemplate = null;

	public void setSessionFactory(SessionFactory sessionFactory) {
        this.hibernateTemplate = new HibernateTemplate(sessionFactory);
    }

	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public void insertarPais(Pais pais) {
		this.hibernateTemplate.save(pais);
		
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED, readOnly=true)
	public ArrayList<Pais> obtenerPaises() {
		return (ArrayList<Pais>) this.hibernateTemplate.loadAll(Pais.class);
	}
	
	@Override
	@Transactional(propagation=Propagation.REQUIRED, readOnly=true)
	public Pais obtenerPais(Integer id) {
		return this.hibernateTemplate.get(Pais.class, id);
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRES_NEW)
	public void eliminarPais(Integer idPais) {
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		Pais pais = (Pais)context.getBean("PaisBean");

		pais.setPaisId(idPais);
		this.hibernateTemplate.delete(pais);
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public void actualizarPais(Pais pais) {
		this.hibernateTemplate.update(pais);
	}
	
}
