package frgp.utn.edu.ar.daoImpl;

import java.util.ArrayList;

import org.hibernate.SessionFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import frgp.utn.edu.ar.dao.PeticionDao;
import frgp.utn.edu.ar.dao.PuntoDao;
import frgp.utn.edu.ar.dominio.Peticion;
import frgp.utn.edu.ar.dominio.Punto;

public class PuntoDaoImpl implements PuntoDao{

	private HibernateTemplate hibernateTemplate = null;

	public void setSessionFactory(SessionFactory sessionFactory) {
        this.hibernateTemplate = new HibernateTemplate(sessionFactory);
    }

	
	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public void insertarPunto(Punto punto) {
		this.hibernateTemplate.save(punto);
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED, readOnly=true)
	public ArrayList<Punto> obtenerPuntos(){
		return (ArrayList<Punto>) this.hibernateTemplate.loadAll(Punto.class);
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public void eliminarPunto(Punto punto) {
		this.hibernateTemplate.delete(punto);
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public void actualizarPunto(Punto punto) {
		this.hibernateTemplate.update(punto);
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED, readOnly=true)
	public int obtenerCantidadPuntos(Integer idResena) {
		ArrayList<Punto> lista = null;
		int suma = 0;
		try {
			lista = (ArrayList<Punto>) this.hibernateTemplate.find("FROM Punto WHERE PuntoResena = " + idResena);
		} catch (Exception e) {
			lista = new ArrayList<Punto>();
		}
		
		for(Punto p : lista) {
			if(p.getPuntoPositivo())
				suma++;
			else
				suma--;
		}
		
		return suma;
	}
	
	@Override
	@Transactional(propagation=Propagation.REQUIRED, readOnly=true)
	public ArrayList<Punto> obtenerPuntosUsuario(Integer idUsuario){
		try {
			return (ArrayList<Punto>) this.hibernateTemplate.find("FROM Punto WHERE PuntoUsuario = " + idUsuario);
		} catch (Exception e) {
			return new ArrayList<Punto>();
		}
	}
	
	@Override
	@Transactional(propagation=Propagation.REQUIRED, readOnly=true)
	public ArrayList<Punto> obtenerPunto(Integer idResena, Integer idUsuario){
		try {
			return (ArrayList<Punto>) this.hibernateTemplate.find("FROM Punto WHERE PuntoResena = " + idResena + " AND PuntoUsuario = " + idUsuario);
		} catch (Exception e) {
			return new ArrayList<Punto>();
		}
	}
}
