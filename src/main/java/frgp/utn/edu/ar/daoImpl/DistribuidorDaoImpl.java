package frgp.utn.edu.ar.daoImpl;

import java.util.ArrayList;

import org.hibernate.SessionFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import frgp.utn.edu.ar.dao.DistribuidorDao;
import frgp.utn.edu.ar.dominio.Desarrollador;
import frgp.utn.edu.ar.dominio.Distribuidor;

public class DistribuidorDaoImpl implements DistribuidorDao{

	private HibernateTemplate hibernateTemplate = null;

	public void setSessionFactory(SessionFactory sessionFactory) {
        this.hibernateTemplate = new HibernateTemplate(sessionFactory);
    }

	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public void insertarDistribuidor(Distribuidor distribuidor) {
		this.hibernateTemplate.save(distribuidor);
		
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED, readOnly=true)
	public ArrayList<Distribuidor> obtenerDistribuidores() {
		return (ArrayList<Distribuidor>) this.hibernateTemplate.loadAll(Distribuidor.class);
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRES_NEW)
	public void eliminarDistribuidor(Integer idDistribuidor) {
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		Distribuidor distribuidor = (Distribuidor)context.getBean("DistribuidorBean");

		distribuidor.setDistribuidorId(idDistribuidor);
		this.hibernateTemplate.delete(distribuidor);
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public void actualizarDistribuidor(Distribuidor distribuidor) {
		this.hibernateTemplate.update(distribuidor);
	}
	
	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public Distribuidor obtenerDistribuidor(Integer id) {
		return (Distribuidor) this.hibernateTemplate.get(Distribuidor.class, id);
	}
	
}
