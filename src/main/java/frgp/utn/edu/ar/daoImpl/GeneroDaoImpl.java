package frgp.utn.edu.ar.daoImpl;

import java.util.ArrayList;

import org.hibernate.SessionFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import frgp.utn.edu.ar.dao.GeneroDao;
import frgp.utn.edu.ar.dominio.Genero;

public class GeneroDaoImpl implements GeneroDao {

	private HibernateTemplate hibernateTemplate = null;

	public void setSessionFactory(SessionFactory sessionFactory) {
        this.hibernateTemplate = new HibernateTemplate(sessionFactory);
    }

	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public void insertarGenero(Genero genero) {
		this.hibernateTemplate.save(genero);
		
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED, readOnly=true)
	public ArrayList<Genero> obtenerGeneros() {
		return (ArrayList<Genero>) this.hibernateTemplate.loadAll(Genero.class);
	}
	
	@Override
	@Transactional(propagation=Propagation.REQUIRED, readOnly=true)
	public Genero obtenerGenero(Integer id) {
		return this.hibernateTemplate.get(Genero.class, id);
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRES_NEW)
	public void eliminarGenero(Integer idGenero) {
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		Genero genero = (Genero)context.getBean("GeneroBean");

		genero.setGeneroId(idGenero);
		this.hibernateTemplate.delete(genero);
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public void actualizarGenero(Genero genero) {
		this.hibernateTemplate.update(genero);
	}
	
}
