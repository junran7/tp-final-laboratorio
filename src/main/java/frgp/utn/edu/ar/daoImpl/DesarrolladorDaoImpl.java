package frgp.utn.edu.ar.daoImpl;

import java.util.ArrayList;

import org.hibernate.SessionFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import frgp.utn.edu.ar.dao.DesarrolladorDao;
import frgp.utn.edu.ar.dominio.Desarrollador;

public class DesarrolladorDaoImpl implements DesarrolladorDao {
	
	private HibernateTemplate hibernateTemplate = null;

	public void setSessionFactory(SessionFactory sessionFactory) {
        this.hibernateTemplate = new HibernateTemplate(sessionFactory);
    }

	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public void insertarDesarrollador(Desarrollador desarrollador) {
		this.hibernateTemplate.save(desarrollador);
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED, readOnly=true)
	public ArrayList<Desarrollador> obtenerDesarrolladores() {
			return (ArrayList<Desarrollador>) this.hibernateTemplate.loadAll(Desarrollador.class);
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRES_NEW)
	public void eliminarDesarrollador(Integer idDesarrollador) {
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		Desarrollador desarrollador = (Desarrollador)context.getBean("DesarrolladorBean");

		desarrollador.setDesarrolladorId(idDesarrollador);
		this.hibernateTemplate.delete(desarrollador);
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public void actualizarDesarrollador(Desarrollador desarrollador) {
		this.hibernateTemplate.update(desarrollador);
	}
	
	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public Desarrollador obtenerDesarrollador(Integer idDesarrollador) {
		return (Desarrollador) this.hibernateTemplate.get(Desarrollador.class, idDesarrollador);
	}

}
