package frgp.utn.edu.ar.daoImpl;

import java.util.ArrayList;

import org.hibernate.SessionFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import frgp.utn.edu.ar.dao.ResenaDao;
import frgp.utn.edu.ar.dominio.Resena;

public class ResenaDaoImpl implements ResenaDao{

	private HibernateTemplate hibernateTemplate = null;

	public void setSessionFactory(SessionFactory sessionFactory) {
        this.hibernateTemplate = new HibernateTemplate(sessionFactory);
    }

	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public void insertarResena(Resena resena) {
		this.hibernateTemplate.save(resena);
		
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED, readOnly=true)
	public ArrayList<Resena> obtenerResenas() {
		return (ArrayList<Resena>) this.hibernateTemplate.loadAll(Resena.class);
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED, readOnly=true)
	public ArrayList<Resena> obtenerResenasPorJuego(Integer idJuego) {
		try {
			return (ArrayList<Resena>) this.hibernateTemplate.find("FROM Resena WHERE JuegoId = " + idJuego);
		} catch (Exception e) {
			return new ArrayList<Resena>();
		}
		 
	}
	
	@Override
	@Transactional(propagation=Propagation.REQUIRES_NEW)
	public void eliminarResena(Integer idResena) {
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		Resena resena = (Resena)context.getBean("ResenaBean");

		resena.setResenaId(idResena);
		this.hibernateTemplate.delete(resena);
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public void actualizarResena(Resena resena) {
		this.hibernateTemplate.update(resena);
	}
	
	@Override
	public Resena obtenerResena(Integer resenaId) {
		return this.hibernateTemplate.get(Resena.class, resenaId);
	}
	
	@Override
	@Transactional(propagation=Propagation.REQUIRED, readOnly=true)
	public ArrayList<Resena> obtenerResenasPorJuegoYUsuario(Integer idJuego, Integer idUsuario) {
		try {
			return (ArrayList<Resena>) this.hibernateTemplate.find("FROM Resena WHERE ResenaJuego = " + idJuego + " AND ResenaUsuario = " + idUsuario);
		} catch (Exception e) {
			return new ArrayList<Resena>();
		}
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED, readOnly=true)
	public Double obtenerSumaPuntaje(Integer idJuego) {
		ArrayList<Resena> sdfa;
		try 
		{
			sdfa = (ArrayList<Resena>)this.hibernateTemplate.find("FROM Resena WHERE JuegoId = " + idJuego);
			double d = 0;
			for(Resena r : sdfa) {
				d += r.getResenaPuntaje();
			}
			return d;
		}
		catch(Exception e)
		{
			return 0.0D;
		}
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED, readOnly=true)
	public Double obtenerCantidadResenas(Integer idJuego) {
		ArrayList<Resena> asdf;
		asdf = (ArrayList<Resena>)this.hibernateTemplate.find("FROM Resena WHERE JuegoId = " + idJuego);
		return (double) asdf.size();
	}

}
