package frgp.utn.edu.ar.daoImpl;

import java.util.ArrayList;

//import org.apache.catalina.core.ApplicationContext;
import org.springframework.context.ApplicationContext;
import org.hibernate.SessionFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import frgp.utn.edu.ar.dao.UsuarioDao;
import frgp.utn.edu.ar.dominio.Usuario;

public class UsuarioDaoImpl implements UsuarioDao
{

	private HibernateTemplate hibernateTemplate = null;

	public void setSessionFactory(SessionFactory sessionFactory)
	{
		this.hibernateTemplate = new HibernateTemplate(sessionFactory);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void insertarUsuario(Usuario usuario)
	{
		this.hibernateTemplate.save(usuario);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public Usuario obtenerUsuarioPorNombre(String nombreUser)
	{
		return this.hibernateTemplate.get(Usuario.class, nombreUser);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public ArrayList<Usuario> obtenerUsuarios()
	{
		return (ArrayList<Usuario>) this.hibernateTemplate.loadAll(Usuario.class);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void eliminarUsuario(Integer idUsuario)
	{
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		Usuario user = (Usuario) context.getBean("UsuarioBean");

		user.setUsuarioId(idUsuario);
		this.hibernateTemplate.delete(user);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void actualizarUsuario(Usuario persona)
	{
		this.hibernateTemplate.update(persona);
	}

	@Override
	public boolean existeEmail(String email)
	{

		if (this.hibernateTemplate.find("FROM Usuario WHERE UsuarioEmail LIKE '%" + email + "%'").isEmpty())

			return false;

		else

			return true;
	}

	@Override
	public Usuario obtenerUsuarioLogin(String mail, String contrasena)
	{
		try
		{
			return (Usuario) this.hibernateTemplate.find("FROM Usuario WHERE UsuarioEmail LIKE '" + mail
					+ "' AND UsuarioContrasena LIKE '" + contrasena + "'").get(0);
		} catch (Exception e)
		{
			throw e;
		}

	}

	@Override
	public boolean existeEmail(String email, int id)
	{
		if (this.hibernateTemplate.find("FROM Usuario WHERE UsuarioEmail LIKE '%" + email + "%' AND UsuarioId <> " + id)
				.isEmpty())

			return false;

		else

			return true;
	}

	@Override
	public boolean contrasenaValida(int id, String pass)
	{
		if (this.hibernateTemplate
				.find("FROM Usuario WHERE UsuarioId = " + id + " AND UsuarioContrasena LIKE '" + pass + "'").isEmpty())
			return false;
		else
			return true;
	}

	@Override
	public Usuario obtenerUsuario(int id)
	{
		return this.hibernateTemplate.get(Usuario.class, id);
	}
}
