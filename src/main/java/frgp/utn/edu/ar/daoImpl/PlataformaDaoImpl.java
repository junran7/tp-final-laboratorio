package frgp.utn.edu.ar.daoImpl;

import java.util.ArrayList;

import org.hibernate.SessionFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import frgp.utn.edu.ar.dao.PlataformaDao;
import frgp.utn.edu.ar.dominio.Plataforma;

public class PlataformaDaoImpl implements PlataformaDao{

	private HibernateTemplate hibernateTemplate = null;

	public void setSessionFactory(SessionFactory sessionFactory) {
        this.hibernateTemplate = new HibernateTemplate(sessionFactory);
    }

	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public void insertarPlataforma(Plataforma plataforma) {
		this.hibernateTemplate.save(plataforma);
		
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED, readOnly=true)
	public ArrayList<Plataforma> obtenerPlataformas() {
		return (ArrayList<Plataforma>) this.hibernateTemplate.loadAll(Plataforma.class);
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRES_NEW)
	public void eliminarPlataforma(Integer idPlataforma) {
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		Plataforma plataforma = (Plataforma)context.getBean("PlataformaBean");

		plataforma.setPlataformaId(idPlataforma);
		this.hibernateTemplate.delete(plataforma);
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public void actualizarPlataforma(Plataforma plataforma) {
		this.hibernateTemplate.update(plataforma);
	}
	
	@Override
	@Transactional(propagation=Propagation.REQUIRED, readOnly=true)
	public Plataforma obtenerPlataforma(Integer idPlataforma) {
		return (Plataforma) this.hibernateTemplate.get(Plataforma.class, idPlataforma);
	}
	
}
