package frgp.utn.edu.ar.controllers;
import java.text.DateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.servlet.ServletConfig;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;


import org.springframework.web.bind.annotation.PathVariable;

import frgp.utn.edu.ar.dominio.Desarrollador;
import frgp.utn.edu.ar.dominio.Distribuidor;
import frgp.utn.edu.ar.dominio.Juego;
import frgp.utn.edu.ar.servicio.JuegoServicio;
import frgp.utn.edu.ar.dominio.Pais;
import frgp.utn.edu.ar.dominio.Usuario;
import frgp.utn.edu.ar.servicio.UsuarioServicio;

@Controller
@SessionAttributes("User")
public class UserController {

	@Autowired
	public  UsuarioServicio service;
	
	public void init(ServletConfig config) {
		ApplicationContext ctx = WebApplicationContextUtils
				.getRequiredWebApplicationContext(config.getServletContext());
		
		this.service = (UsuarioServicio) ctx.getBean("serviceUsuarioBean");
	}
	
	
	//Inicio
	
	@RequestMapping("/abrirUsuarios.html")
	public ModelAndView redireccion(){
		ModelAndView MV = new ModelAndView();
		MV.addObject("listaUsuarios",this.service.obtenerUsuarios());
		MV.setViewName("Usuarios"); 
		return MV;
	}
	
	
	@RequestMapping(value ="/altaUsuario.html" , method= { RequestMethod.GET, RequestMethod.POST})
	public ModelAndView validarUsuario(String nombreU, String passU){
		ModelAndView MV = new ModelAndView();
		
		Usuario x = new Usuario();
		x.setUsuarioNombre(nombreU);
		x.setUsuarioContrasena(passU);
		//para probar
		x.setUsuarioBio("test");
		x.setUsuarioEmail("test@test.com");
		x.setUsuarioFechacreacion("HOY");
		x.setUsuarioAdmin(true);
		x.setUsuarioBaneado(false);
		x.setUsuarioBorrado(false);
				
		String Message="";
		
		try{
			
			service.insertarUsuario(x);
			Message = "Usuario agregado";
		}
		catch(Exception e)
		{
			Message = "No se pudo insertar el usuario";
		}
		finally
		{
		
		}
	
		MV.setViewName("Usuarios");
		MV.addObject("Mensaje", Message);
		MV.addObject("listaUsuarios",this.service.obtenerUsuarios());
		MV.setViewName("Usuarios"); 
		return MV;
		
	}
	
     
	@RequestMapping(value ="EliminarUsuario.html" , method= { RequestMethod.GET, RequestMethod.POST})
	public ModelAndView eliminarUsuario(Integer id){
		ModelAndView MV = new ModelAndView();
		service.eliminarUsuario(id);
		MV.addObject("listaUsuarios",this.service.obtenerUsuarios());
		MV.setViewName("Admin/ListadoAdmin"); 
		return MV;
	}
	
	@RequestMapping(value ="BanearUsuario.html" , method= { RequestMethod.GET, RequestMethod.POST})
	public ModelAndView banearUsuario(Integer id){
		ModelAndView MV = new ModelAndView();
		Usuario user = service.obtenerUsuario(id);
		service.banearUsuario(user);
		MV.addObject("listaUsuarios",this.service.obtenerUsuarios());
		MV.setViewName("Admin/ListadoAdmin"); 
		return MV;
	}
	
	@RequestMapping(value ="DesbanearUsuario.html" , method= { RequestMethod.GET, RequestMethod.POST})
	public ModelAndView desbanearUsuario(Integer id){
		ModelAndView MV = new ModelAndView();
		Usuario user = service.obtenerUsuario(id);
		service.desbanearUsuario(user);
		MV.addObject("listaUsuarios",this.service.obtenerUsuarios());
		MV.setViewName("Admin/ListadoAdmin"); 
		return MV;
	}
	
	@RequestMapping(value ="AdminearUsuario.html" , method= { RequestMethod.GET, RequestMethod.POST})
	public ModelAndView adminearUsuario(Integer id){
		ModelAndView MV = new ModelAndView();
		Usuario user = service.obtenerUsuario(id);
		service.adminearUsuario(user);
		MV.addObject("listaUsuarios",this.service.obtenerUsuarios());
		MV.setViewName("Admin/ListadoAdmin"); 
		return MV;
	}
	
	@RequestMapping(value = { "/delete-user-{ssoId}" }, method = RequestMethod.GET)
    public ModelAndView deleteUser(@PathVariable Integer ssoId) {
		service.eliminarUsuario(ssoId);
		ModelAndView MV = new ModelAndView();
		MV.setViewName("Usuarios");
		
		//Actualiza los usuarios
		MV.addObject("listaUsuarios",this.service.obtenerUsuarios());
		MV.setViewName("Usuarios"); 
		return MV;
    }
	
	@RequestMapping(value ="/recargaGrillaUsuarios.html" , method= { RequestMethod.GET, RequestMethod.POST})
	public ModelAndView recargarUsuario(){
		ModelAndView MV = new ModelAndView();
		MV.addObject("listaUsuarios",this.service.obtenerUsuarios());
		MV.setViewName("Usuarios"); 
		return MV;
		
	}
	
	@RequestMapping(value ="testJuego.html" , method= { RequestMethod.GET, RequestMethod.POST})
	public void testJuego(){
		
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		Juego juego = (Juego)context.getBean("JuegoBean");
		
		Pais pais = new Pais();
		pais.setPaisNombre("arg");
		
		Desarrollador des = new Desarrollador();
		des.setDesarrolladorNombre("dev1");
		des.setDesarrolladorPais(pais);
		des.setDesarrolladorFechaCreacion("hoy");
		
		Distribuidor dis = new Distribuidor();
		dis.setDistribuidorNombre("dis1");
		dis.setDistribuidorPais(pais);
		dis.setDistribuidorCreacion("hoy");
		
		
		
		
		
		juego.setJuegoNombre("asd");
		juego.setJuegoDescripcion("asd");
		juego.setJuegoDesarrollador(des);
		juego.setJuegoDistribuidor(dis); 
		juego.setJuegoFechaLanzamiento("ayer");
		
		
		System.out.println(juego.toString());
		/*
		String Message="";
		
		try{
			
			//service.insertarJuego(juego);
			Message = "Juego agregado";
			System.out.println(juego.toString());
		}
		catch(Exception e)
		{
			Message = "No se pudo insertar el juego";
			System.out.println(e.toString());
		}
		finally
		{
			System.out.println(Message);
		}
		*/
		
	}
		
	@RequestMapping("Registrar.html")
	public ModelAndView Registrar(HttpServletRequest request, String username, String email, String pass, String pass2) {
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
		LocalDate localDate = LocalDate.now();
		
		Usuario user = (Usuario)context.getBean("UsuarioBean");
		user.setUsuarioNombre(username);
		user.setUsuarioEmail(email);
		user.setUsuarioContrasena(pass);
		user.setUsuarioAdmin(false);
		user.setUsuarioBaneado(false);
		user.setUsuarioBorrado(false);
		user.setUsuarioFechacreacion(dtf.format(localDate));
		user.setUsuarioBio("");
		
		String respuesta = "";
		ModelAndView MV = new ModelAndView();
		
		if(pass.equals(pass2))
		{
			if(this.service.existeEmail(email))
				respuesta = "El Email ingresado ya se encuentra registrado.";
			else
				respuesta = this.service.registrarUsuario(user);
			MV.addObject("respuesta", respuesta);
			if(respuesta.contains("error") || respuesta.contains("ya se encuentra registrado"))
				MV.setViewName("Usuario/Registro");
			else
				MV.setViewName("Usuario/Login");
		}
		else
		{
			respuesta = "Las contrase�as deben coincidir.";
			MV.addObject("respuesta", respuesta);		
			MV.setViewName("Usuario/Registro");
		}
		return MV;
		
	}
	
	@RequestMapping("Logearse.html")
	public ModelAndView Login(ModelMap model, HttpServletRequest request, String email, String pass) {
		ModelAndView MV = new ModelAndView();
		try
		{
			Usuario user = this.service.obtenerUsuarioLogin(email, pass);
			model.addAttribute("User", user);
			request.setAttribute("User", user);
			MV.setViewName("Usuario/Inicio");
		}
		catch(Exception e)
		{
			request.setAttribute("respuesta", "Email o contrase�a no incorrecta.");
			MV.setViewName("Usuario/Login");
		}
		
		return MV;
	}
	
	@RequestMapping("Desconectar.html")
	public ModelAndView Desconectar(ModelMap model, HttpServletRequest request) {
		ModelAndView MV = new ModelAndView();
		model.addAttribute("User", new Usuario());
		request.setAttribute("User", null);
		MV.setViewName("Usuario/Inicio");
		return MV;
	}
	
	@RequestMapping("ActualizarDatos.html")
	public ModelAndView ActualizarDatos(HttpServletRequest request, String user, String mail, String bio)
	{
		ModelAndView MV = new ModelAndView();
		String respuesta = "";
		
		Usuario usu = (Usuario)request.getSession().getAttribute("User");
		
		if(this.service.existeEmail(mail, usu.getUsuarioId()))
			respuesta = "El Email ingresado ya se encuentra registrado.";
		else
		{
			usu.setUsuarioNombre(user);
			usu.setUsuarioEmail(mail);
			usu.setUsuarioBio(bio);
			try
			{
				this.service.actualizarUsuario(usu);
				respuesta = "Se han guardado los datos con �xito!";

				request.setAttribute("User", usu);
			}
			catch(Exception e)
			{
				respuesta = "Ha ocurrido un error.";
			}
			
		}
		request.setAttribute("respuesta", respuesta);
		MV.setViewName("Usuario/MisDatos");
		return MV;
	}
	
	@RequestMapping("CambiarseContrasena.html")
	public ModelAndView CambiarContrasena(HttpServletRequest request, String passActual, String passNueva)
	{
		ModelAndView MV = new ModelAndView();
		String respuesta = "";
		Usuario usu = (Usuario)request.getSession().getAttribute("User");
		
//		Primero validar que la pass actual sea la correcta
//		Despues validar si son iguales, la nueva y la vieja
		if(this.service.contrasenaValida(usu.getUsuarioId(), passActual))
		{
			if(passActual.equals(passNueva))
			{
				respuesta = "Tu nueva contrase�a no puede ser la misma que la actual.";
			}
			else
			{
				try
				{
					usu.setUsuarioContrasena(passNueva);
					this.service.actualizarUsuario(usu);
					respuesta = "Se han guardado los datos con �xito!";

					request.setAttribute("User", usu);
				}
				catch(Exception e)
				{
					respuesta = "Ha ocurrido un error.";
				}
			}
		}
		else
			respuesta = "Contrase�a actual incorrecta.";
		
		
		
		request.setAttribute("respuesta", respuesta);
		MV.setViewName("Usuario/CambiarContrasena");
		return MV;
	}
	
	@RequestMapping("ListarUsuariosAdmin.html")
	public ModelAndView IrListadoAdmin() {
		ModelAndView MV = new ModelAndView();
		MV.addObject("listaUsuarios",this.service.obtenerUsuarios());
		MV.setViewName("Admin/ListadoAdmin");
		return MV;
	}
		
}
