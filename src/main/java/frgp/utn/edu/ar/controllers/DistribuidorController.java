package frgp.utn.edu.ar.controllers;

import javax.servlet.ServletConfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;

import frgp.utn.edu.ar.dominio.Pais;
import frgp.utn.edu.ar.servicio.PaisServicio;
import org.springframework.web.bind.annotation.RequestMethod;
import frgp.utn.edu.ar.dominio.Distribuidor;
import frgp.utn.edu.ar.servicio.DistribuidorServicio;

@Controller
public class DistribuidorController {

	@Autowired
	public DistribuidorServicio service;
	@Autowired
	public PaisServicio servicePais;
	
//	public void init(ServletConfig config) {
//		ApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(config.getServletContext());
//		this.servicePais = (PaisServicio) ctx.getBean("servicePaisBean");
//		this.service = (DistribuidorServicio) ctx.getBean("serviceDistribuidorBean");
//	}
	
	@RequestMapping("ABMLDistribuidores.html")
	public ModelAndView ABMLDistribuidores() {
	    ModelAndView MV = new ModelAndView();
	    MV.addObject("listaDistribuidores",this.service.obtenerDistribuidores());
	    MV.addObject("listaPaises", this.servicePais.obtenerPaises());
	    MV.setViewName("ABML/ABMLDistribuidores");
	    return MV;
	}
	
	@RequestMapping(value="ModificarDistribuidor.html", params = "eliminarDis")
	public ModelAndView EliminarDistribuidor(Integer distribuidores) {
	    ModelAndView MV = new ModelAndView();
	    Distribuidor distribuidor = service.obtenerDistribuidor(distribuidores);
	    String mensaje = "Eliminado exitoso.";
	    if(distribuidor != null){
	    	try {
	    		service.eliminarDistribuidor(distribuidores);
		    }
		    catch(Exception e) {
		    	mensaje = "El Distribuidor no puede ser eliminado ya que est� en uso.";
		    }
	    }
	    MV.addObject("mensajeResultado", mensaje);
	    MV.addObject("listaDistribuidores",this.service.obtenerDistribuidores());
	    MV.addObject("listaPaises", this.servicePais.obtenerPaises());
	    MV.setViewName("ABML/ABMLDistribuidores");
	    return MV;
	}
	
	@RequestMapping(value="ModificarDistribuidor.html", params = "modificarDis")
	public ModelAndView ModificarDistribuidor(Integer distribuidores) {
	    ModelAndView MV = new ModelAndView();
	    Distribuidor distribuidor = service.obtenerDistribuidor(distribuidores);
	    if(distribuidor != null)
	    	MV.addObject("distribuidorAModif", distribuidor);
	    MV.addObject("listaDistribuidores",this.service.obtenerDistribuidores());
	    MV.addObject("listaPaises", this.servicePais.obtenerPaises());
	    MV.setViewName("ABML/ABMLDistribuidores");
	    return MV;
	}
	
	@RequestMapping("AgregarDistribuidor.html")
	public ModelAndView agregarDistribuidor(String idDis, String nameDis, String createdDis, String paisDis) {
	    ModelAndView MV = new ModelAndView();
	    
	    ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		Distribuidor distribuidor = (Distribuidor)context.getBean("DistribuidorBean");
		
		if(idDis != "") {
			distribuidor = this.service.obtenerDistribuidor(Integer.parseInt(idDis));	
			distribuidor.setDistribuidorNombre(nameDis);
			distribuidor.setDistribuidorCreacion(createdDis);
			distribuidor.setDistribuidorPais(servicePais.obtenerPais(Integer.parseInt(paisDis)));
			this.service.actualizarDistribuidor(distribuidor);
		}
		else {
			distribuidor.setDistribuidorNombre(nameDis);
			distribuidor.setDistribuidorCreacion(createdDis);
			distribuidor.setDistribuidorPais(servicePais.obtenerPais(Integer.parseInt(paisDis)));
			this.service.insertarDistribuidor(distribuidor);
		}

	    MV.addObject("listaDistribuidores",this.service.obtenerDistribuidores());
	    MV.addObject("listaPaises", this.servicePais.obtenerPaises());
	    MV.setViewName("ABML/ABMLDistribuidores");
	    return MV;
	}
	
	@RequestMapping("/abrirDistribuidores.html")
	public ModelAndView redireccion(){
		ModelAndView MV = new ModelAndView();
		MV.addObject("listaDistribuidores",this.service.obtenerDistribuidores());
		MV.addObject("listaPaises", this.servicePais.obtenerPaises());
		MV.setViewName("Distribuidores"); 
		return MV;
	}
	
	
	@RequestMapping(value ="/altaDistribuidor.html" , method= { RequestMethod.GET, RequestMethod.POST})
	public ModelAndView validarDistribuidor(String nombreD, String creacionD, Pais paisD){
		ModelAndView MV = new ModelAndView();
		
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		Distribuidor x = (Distribuidor)context.getBean("DistribuidorBean");	
		//tal vez si persistimos el objeto que hicimos con el bean y despues hacemos context.close() no se borra?
		x.setDistribuidorNombre(nombreD);
		x.setDistribuidorCreacion(creacionD);
		x.setDistribuidorPais(paisD);
		
		String Message="";
		
		try{
			
			service.insertarDistribuidor(x);
			Message = "Distribuidor agregado";
		}
		catch(Exception e)
		{
			Message = "No se pudo insertar el distribuidor";
		}
		finally
		{
		
		}
	
		MV.setViewName("Distribuidores");
		MV.addObject("Mensaje", Message);
		MV.addObject("listaDistribuidores",this.service.obtenerDistribuidores());
		//listapaises?
		MV.setViewName("Distribuidores"); 
		return MV;
		
	}
	
     
	@RequestMapping(value ="/eliminarDistribuidor.html" , method= { RequestMethod.GET, RequestMethod.POST})
	public ModelAndView eliminarDistribuidor(Integer id){
		ModelAndView MV = new ModelAndView();
		service.eliminarDistribuidor(id);
		MV.addObject("listaDistribuidores",this.service.obtenerDistribuidores());
		MV.setViewName("Distribuidores"); 
		MV.addObject("Mensaje", "Distribuidor eliminado");
		return MV;
	}
	/*
	@RequestMapping(value = { "/delete-user-{ssoId}" }, method = RequestMethod.GET)
    public ModelAndView borrarDistribuidor(@PathVariable Integer ssoId) {
		service.eliminarDistribuidor(ssoId);
		ModelAndView MV = new ModelAndView();
		MV.setViewName("Distribuidores");
		
		//Actualiza los generos
		MV.addObject("listaDistribuidores",this.service.obtenerDistribuidores());
		MV.setViewName("Distribuidores"); 
		return MV;
    }*/
	
}
