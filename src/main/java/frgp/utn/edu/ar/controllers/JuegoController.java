package frgp.utn.edu.ar.controllers;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import frgp.utn.edu.ar.dominio.Desarrollador;
import frgp.utn.edu.ar.dominio.Distribuidor;
import frgp.utn.edu.ar.dominio.Genero;
import frgp.utn.edu.ar.dominio.Juego;
import frgp.utn.edu.ar.dominio.Pais;
import frgp.utn.edu.ar.dominio.Plataforma;
import frgp.utn.edu.ar.dominio.Region;
import frgp.utn.edu.ar.dominio.Resena;
import frgp.utn.edu.ar.dominio.Usuario;
import frgp.utn.edu.ar.servicio.DesarrolladorServicio;
import frgp.utn.edu.ar.servicio.DistribuidorServicio;
import frgp.utn.edu.ar.servicio.GeneroServicio;
import frgp.utn.edu.ar.servicio.JuegoServicio;
import frgp.utn.edu.ar.servicio.PlataformaServicio;
import frgp.utn.edu.ar.servicio.PuntoServicio;
import frgp.utn.edu.ar.servicio.ResenaServicio;
import frgp.utn.edu.ar.servicio.UsuarioServicio;

@Controller
public class JuegoController {

	@Autowired
	public  JuegoServicio service;
	@Autowired 
	public PlataformaServicio servicePlat;
	@Autowired
	public GeneroServicio serviceGen;
	@Autowired
	public DistribuidorServicio serviceDis;
	@Autowired
	public DesarrolladorServicio serviceDev;
	@Autowired
	public ServletContext servletContext;
	@Autowired
	public ResenaServicio serviceRes;
	@Autowired
	public UsuarioServicio serviceUser;
	@Autowired
	public PuntoServicio servicePunto;
	
	@RequestMapping(value = "AgregarResena.html", params = "publicarResena")
	public ModelAndView publicarResena(String idGame, String textResena, String calif, String idResena, String idUser, HttpSession session) {
		ModelAndView MV = new ModelAndView();
		
		Usuario user = serviceUser.obtenerUsuario(Integer.parseInt(idUser));
		Resena resena = new Resena();
		
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");  
		System.out.println(formatter.format(date));  
		
		if(!idResena.isEmpty()) {
			resena = serviceRes.obtenerResena(Integer.parseInt(idResena));
			
			System.out.println(resena);
			
			resena.setResenaDescripcion(textResena);
			resena.setResenaPuntaje(Integer.parseInt(calif));
			resena.setResenaFecha(formatter.format(date).toString());
			resena.setResenaUsuario(user);
			resena.setResenaVotos(0);
			serviceRes.actualizarResena(resena);
		} else {
			resena.setResenaDescripcion(textResena);
			resena.setResenaJuego(service.obtenerJuego(Integer.parseInt(idGame)));
			resena.setResenaPuntaje(Integer.parseInt(calif));
			resena.setResenaFecha(formatter.format(date).toString());
			resena.setResenaUsuario(user);
			resena.setResenaVotos(0);
			serviceRes.insertarResena(resena);
		}
		
		Juego juego = service.obtenerJuego(Integer.parseInt(idGame));
		
		juego.setJuegoPromedioResenas(serviceRes.obtenerPromedioNuevo(resena.getResenaPuntaje(), Integer.parseInt(idGame)));		
		service.actualizarJuego(juego);
		
		juego = service.obtenerJuego(Integer.parseInt(idGame));
		
		List<Genero> generos = new ArrayList<>(juego.getlGenero()); 
		List<Plataforma> plataformas = new ArrayList<>(juego.getlPlataformas());
	    
		ArrayList<Resena> resenazxzx = serviceRes.obtenerResenasPorJuegoYUsuario(Integer.parseInt(idGame), Integer.parseInt(idUser));
	    MV.addObject("JuegoAMostrar", juego);
	    if(resena != null && resenazxzx.size() > 0 &&resenazxzx.get(0).getResenaId() != 0) {
	    	MV.addObject("ResenaAMostrar", resenazxzx.get(0));
	    }
		
	    MV.addObject("listaGeneros", generos);
	    MV.addObject("listaPlataformas", plataformas);
		MV.addObject("JuegoAMostrar", juego);
		MV.setViewName("Usuario/Juego");
		return MV;
	}
	
	@RequestMapping(value = "AgregarResena.html", params = "cancelarResena")
	public ModelAndView cancelarResena(String idGame) {
		ModelAndView MV = new ModelAndView();
		
		Juego juego = service.obtenerJuego(Integer.parseInt(idGame));
		
		List<Genero> generos = new ArrayList<>(juego.getlGenero()); 
		List<Plataforma> plataformas = new ArrayList<>(juego.getlPlataformas());
	    
	    MV.addObject("listaGeneros", generos);
	    MV.addObject("listaPlataformas", plataformas);
		MV.addObject("JuegoAMostrar", juego);
		MV.setViewName("Usuario/Juego");
		return MV;
	}
	
	@RequestMapping(value = "JuegoResena.html", params = "escribirResena")
	public ModelAndView EscribirResena(String idGame, String idUser) {
	    ModelAndView MV = new ModelAndView();
	    Juego juego = service.obtenerJuego(Integer.parseInt(idGame));
	    Usuario user = serviceUser.obtenerUsuario(Integer.parseInt(idUser));
	    ArrayList<Resena> resena = serviceRes.obtenerResenasPorJuegoYUsuario(Integer.parseInt(idGame), Integer.parseInt(idUser));
	    MV.addObject("JuegoAMostrar", juego);
	    if(resena != null && resena.size() > 0 &&resena.get(0).getResenaId() != 0) {
	    	MV.addObject("ResenaAMostrar", resena.get(0));
	    }
	    MV.setViewName("Usuario/EscribirResena");
	    return MV;
	}
	
	@RequestMapping(value = "JuegoResena.html", params = "verResenas")
	public ModelAndView verResenas(String idGame, String idUser) {
	    ModelAndView MV = new ModelAndView();
	    Juego juego = service.obtenerJuego(Integer.parseInt(idGame));
	    if(!idUser.isEmpty() && idUser != "") {
	    	Usuario user = serviceUser.obtenerUsuario(Integer.parseInt(idUser));
	    	MV.addObject("User", user);
	    }
	    List<Resena> resenas = new ArrayList<>(serviceRes.obtenerResenas());
	    resenas = resenas.stream().filter(x -> x.getResenaJuego().getJuegoId() == juego.getJuegoId()).collect(Collectors.toList());
	    System.out.println(resenas);
	    MV.addObject("ListaResenas", resenas);
	    MV.addObject("JuegoAMostrar", juego);
	    MV.addObject("UsuarioPuntos", servicePunto.obtenerPuntosUsuario(Integer.parseInt(idUser)));
	    MV.setViewName("Usuario/Resenas");
	    return MV;
	}
	
	@RequestMapping(value = "AgregarJuego1.html", params = "verJuego") // IR BUSCAR
	public ModelAndView VerJuego(String juegos, HttpSession session) {
		ModelAndView MV = new ModelAndView();
		
		Juego juego = service.obtenerJuego(Integer.parseInt(juegos));
		
		List<Genero> generos = new ArrayList<>(juego.getlGenero()); 
		List<Plataforma> plataformas = new ArrayList<>(juego.getlPlataformas());
		
	    MV.addObject("listaGeneros", generos);
	    MV.addObject("listaPlataformas", plataformas);
		MV.addObject("JuegoAMostrar", juego);
		MV.setViewName("Usuario/Juego");
		return MV;
	}
	
	@RequestMapping(value = "mostrarJuego.html", params = "verJuego") // IR BUSCAR
	public ModelAndView IrAlJuego(String idGame) {
		ModelAndView MV = new ModelAndView();
		
		Juego juego = service.obtenerJuego(Integer.parseInt(idGame));
		
		List<Genero> generos = new ArrayList<>(juego.getlGenero()); 
		List<Plataforma> plataformas = new ArrayList<>(juego.getlPlataformas());
	    
	    MV.addObject("listaGeneros", generos);
	    MV.addObject("listaPlataformas", plataformas);
		MV.addObject("JuegoAMostrar", juego);
		MV.setViewName("Usuario/Juego");
		return MV;
	}
	
	@RequestMapping("Buscar.html") // IR BUSCAR
	public ModelAndView Buscar() {
	    ModelAndView MV = new ModelAndView();
	    MV.addObject("listaDesarrolladores", this.serviceDev.obtenerDesarrolladores());
	    MV.addObject("listaDistribuidores", this.serviceDis.obtenerDistribuidores());
	    MV.addObject("listaPlataformas", this.servicePlat.obtenerPlataformas());
	    MV.addObject("listaGeneros", this.serviceGen.obtenerGeneros());
	    MV.setViewName("Usuario/Buscar");
	    return MV;
	}
	
	@RequestMapping("BuscarPeroListarTodo.html") // ir a la pagina de b�squeda pero listando todos los juegos de la BD
	public ModelAndView BuscarPeroListarTodo() {
	    ModelAndView MV = new ModelAndView();
	    MV.addObject("listaDesarrolladores", this.serviceDev.obtenerDesarrolladores());
	    MV.addObject("listaDistribuidores", this.serviceDis.obtenerDistribuidores());
	    MV.addObject("listaPlataformas", this.servicePlat.obtenerPlataformas());
	    MV.addObject("listaGeneros", this.serviceGen.obtenerGeneros());
	    MV.addObject("listaResultados", this.service.obtenerJuegos());
	    MV.setViewName("Usuario/Buscar");
	    return MV;
	}
	
	@RequestMapping("BuscarSimple.html")
	public ModelAndView BuscarSimple(String topSearch) {
	    ModelAndView MV = new ModelAndView();
	    
	    ArrayList<Juego> juegos = service.obtenerJuegosPorNombre(topSearch);
	    
	    MV.addObject("listaResultados", juegos);
	    MV.addObject("topSearch", topSearch);
	    MV.addObject("listaDesarrolladores", this.serviceDev.obtenerDesarrolladores());
	    MV.addObject("listaDistribuidores", this.serviceDis.obtenerDistribuidores());
	    MV.addObject("listaPlataformas", this.servicePlat.obtenerPlataformas());
	    MV.addObject("listaGeneros", this.serviceGen.obtenerGeneros());
	    MV.setViewName("Usuario/Buscar");
	    return MV;
	}
	
	@RequestMapping("BuscarAvanzado.html")
	public ModelAndView BuscarAvanzado(String topSearch, String nombreSearch, String anioSearch, String minPromSearch, String maxPromSearch, String generoSearch, String distribuidorSearch, String desarrolladorSearch, String regionSearch, String plataformaSearch) {
	    ModelAndView MV = new ModelAndView();
	    
	    //ArrayList<Juego> juegos = service.obtenerJuegosPorNombre(topSearch);
	    
	    //ESTO NI SE USA PERO POR LAS DUDAS NO TOQUES NADA
	    
	    //MV.addObject("listaJuegos", juegosAvanzado);
	    MV.addObject("topSearch", topSearch);
	    MV.addObject("listaDesarrolladores", this.serviceDev.obtenerDesarrolladores());
	    MV.addObject("listaDistribuidores", this.serviceDis.obtenerDistribuidores());
	    MV.addObject("listaPlataformas", this.servicePlat.obtenerPlataformas());
	    MV.addObject("listaGeneros", this.serviceGen.obtenerGeneros());
	    MV.setViewName("Usuario/Buscar");
	    return MV;
	}
	
	@RequestMapping(value = "BuscarParametros.html", params = "buscarPlat")
	public ModelAndView BuscarPlataforma(String plat) {
		ModelAndView MV = new ModelAndView();
		MV.addObject("listaDesarrolladores", this.serviceDev.obtenerDesarrolladores());
	    MV.addObject("listaDistribuidores", this.serviceDis.obtenerDistribuidores());
	    MV.addObject("listaPlataformas", this.servicePlat.obtenerPlataformas());
	    MV.addObject("listaGeneros", this.serviceGen.obtenerGeneros());

	    String platSearch = plat;
	    
	    String querybase = "FROM Juego jueg" ;

	    if (!platSearch.contains("322"))
    		//querybase += " INNER JOIN juegosxplataformas JP ON JP.JuegoId = Juego.JuegoId "; 
	    	querybase += " JOIN FETCH jueg.lPlataformas plat ";
	    
	    	querybase+= " WHERE JuegoEstado = 1 ";

	    if (!platSearch.contains("322"))
	    	querybase+= " AND plat.PlataformaId = " + platSearch;

	     System.out.println(querybase);
	    
	    ArrayList<Juego> juegos = service.queryJuego(querybase);
	    System.out.println(juegos.getClass());
	    
	    System.out.println(juegos.toString());
	    System.out.println("--------------");
	    Object[]items = juegos.toArray();
	    
	    for (Object item : items) {
	        System.out.println(item);
	    } 
	    MV.addObject("listaResultados", juegos);
	    MV.setViewName("Usuario/Buscar");
	    return MV;
	}
	
	@RequestMapping(value = "BuscarParametros.html", params = "buscarParametros")
	public ModelAndView BuscarParametros(String nombre, Integer fecha, String genero, String plat, String dev, String pub, String region, String minprom, String maxprom) {
		ModelAndView MV = new ModelAndView();
		MV.addObject("listaDesarrolladores", this.serviceDev.obtenerDesarrolladores());
	    MV.addObject("listaDistribuidores", this.serviceDis.obtenerDistribuidores());
	    MV.addObject("listaPlataformas", this.servicePlat.obtenerPlataformas());
	    MV.addObject("listaGeneros", this.serviceGen.obtenerGeneros());
	    
		//ArrayList<Juego> juegos = service.obtenerJuegosPorNombre(topSearch);
		//	    String nombre = "";
	    String fechaSearch;
	    if(fecha == null)
	    	fechaSearch = "0";
	    else
	    fechaSearch = fecha.toString();
	    
	    String generoSearch = genero;
	    String platSearch = plat;
	    String devSearch = dev;
	    String pubSearch = pub;
	    
	    String querybase = "FROM Juego jueg" ;

	    //USO 322 PORQUE SI VALIDO QUE SEA DISTINTO DE 0 (COMO HACIA ANTES) LE DA IGUAL Y HACE LO QUE QUIERDFKDFH
	    if (!generoSearch.contains("322"))
    		//querybase += " INNER JOIN juegoxgenero JG ON JG.JuegoId = Juego.JuegoId ";
	    	querybase += " JOIN FETCH jueg.lGenero g ";
	    
	    if (!platSearch.contains("322"))
    		//querybase += " INNER JOIN juegosxplataformas JP ON JP.JuegoId = Juego.JuegoId "; 
	    	querybase += " JOIN FETCH jueg.lPlataformas plat ";
	    
	    	querybase+= " WHERE JuegoEstado = 1 ";
	    
	    if (!generoSearch.contains("322"))
	    	querybase+= " AND g.GeneroId = " + generoSearch;
	    
	    if (!platSearch.contains("322"))
	    	querybase+= " AND plat.PlataformaId = " + platSearch;
	    
	    if (!pubSearch.contains("322"))
	    	querybase += " AND DistribuidorId = " + pubSearch;
	    
	    if (!devSearch.contains("322"))
	    	querybase += " AND DesarrolladorId = " + devSearch;	

	    if(nombre != "")		
	    	querybase += " AND JuegoNombre LIKE '%" + nombre + "%'";
	    	
	    
	    if(fechaSearch != "0" && Integer.parseInt(fechaSearch) <= 2019 && Integer.parseInt(fechaSearch) >= 1975)
	    	querybase += " AND JuegoFechaLanzamiento BETWEEN '" + fechaSearch + "-01-01' AND '" + fechaSearch + "-12-31'";
	    querybase += " AND JuegoPromedioResenas BETWEEN " + (minprom != "" ? minprom : 0) + " AND " + (maxprom != "" ? maxprom : 10);
	    	
	    
	    switch(region)
	    {
	    case "1": querybase += " AND regionUS = 1 ";
	    break;
	    case "2": querybase += " AND regionEU = 1 ";
	    break;
	    case "3": querybase += " AND regionJP = 1 ";
	    break;
	    default: querybase += "";
	    }
	     
	     System.out.println(querybase);
	    
	    ArrayList<Juego> juegos = service.queryJuego(querybase);
	    System.out.println(juegos.getClass());
	    
	    System.out.println(juegos.toString());
	    System.out.println("--------------");
	    Object[]items = juegos.toArray();
	    
	    for (Object item : items) {
	        System.out.println(item);
	    } 
	    MV.addObject("listaResultados", juegos);
	    MV.setViewName("Usuario/Buscar");
	    return MV;
	}
	
//	@RequestMapping(value = "BuscarParametros.html", params = "buscarParametros")
//	public ModelAndView BuscarParametros() {
//		ModelAndView MV = new ModelAndView();
//		MV.addObject("listaDesarrolladores", this.serviceDev.obtenerDesarrolladores());
//	    MV.addObject("listaDistribuidores", this.serviceDis.obtenerDistribuidores());
//	    MV.addObject("listaPlataformas", this.servicePlat.obtenerPlataformas());
//	    MV.addObject("listaGeneros", this.serviceGen.obtenerGeneros());
//	    MV.setViewName("Usuario/Buscar");
//	    return MV;
//	}
//	
	@RequestMapping(value = "BuscarParametros.html", params = "buscarLimpiar")
	public ModelAndView BuscarParametrosLimpiar() {
		ModelAndView MV = new ModelAndView();
		MV.addObject("listaDesarrolladores", this.serviceDev.obtenerDesarrolladores());
	    MV.addObject("listaDistribuidores", this.serviceDis.obtenerDistribuidores());
	    MV.addObject("listaPlataformas", this.servicePlat.obtenerPlataformas());
	    MV.addObject("listaGeneros", this.serviceGen.obtenerGeneros());
	    MV.setViewName("Usuario/Buscar");
	    return MV;
	}
	
	@RequestMapping("AgregarJuego.html")
	public ModelAndView ABMLJuegos() {
	    ModelAndView MV = new ModelAndView();
	    MV.addObject("listaJuegos", this.service.obtenerJuegos());
	    MV.setViewName("ABML/ABMLJuegos");
	    return MV;
	}
	
	@RequestMapping(value= "AgregarJuego1.html", params= "nuevoJuego")
	public ModelAndView NuevoJuego() {
	    ModelAndView MV = new ModelAndView();
	    MV.addObject("listaDesarrolladores", this.serviceDev.obtenerDesarrolladores());
	    MV.addObject("listaDistribuidores", this.serviceDis.obtenerDistribuidores());
	    MV.addObject("listaJuegos", this.service.obtenerJuegos());
	    MV.setViewName("ABML/AgregarJuego");
	    return MV;
	}
	
	@RequestMapping(value= "AgregarJuego1.html", params= "modificarJuego")
	public ModelAndView ModificarJuego(Integer juegos) {
	    ModelAndView MV = new ModelAndView();
	    if(juegos != null) {
	    	Juego juego = this.service.obtenerJuego(juegos);
		    if(juego != null)
		    	MV.addObject("juegoAModif", juego);
		    MV.addObject("listaDesarrolladores", this.serviceDev.obtenerDesarrolladores());
		    MV.addObject("listaDistribuidores", this.serviceDis.obtenerDistribuidores());
		    MV.setViewName("ABML/AgregarJuego");
	    }
	    else {
	    	MV.addObject("listaJuegos", this.service.obtenerJuegos());
	    	MV.addObject("mensajeResultado", new String("Debe seleccionar un juego para modificar."));
	    }
	    
	    return MV;
	}
	
	@RequestMapping(value= "AgregarJuego1.html", params= "eliminarJuego")
	public ModelAndView EliminarJuego(Integer juegos) {
	    ModelAndView MV = new ModelAndView();
	    Juego juego = this.service.obtenerJuego(juegos);
	    String mensaje = "Eliminado Exitoso.";
	    if(juego != null)
	    	try {
	    		service.eliminarJuego(juegos);
		    }
		    catch(Exception e) {
		    	mensaje = "El Juego no puede ser eliminado ya que est� en uso.";
		    }
	    MV.addObject("mensajeResultado", mensaje);
	    MV.addObject("listaJuegos", this.service.obtenerJuegos());
	    MV.setViewName("ABML/ABMLJuegos");
	    return MV;
	}
	
	@RequestMapping(value= "AgregarJuego2.html", params= "volverJuego")
	public ModelAndView CancelarJuego() {
	    ModelAndView MV = new ModelAndView();
	    MV.addObject("listaJuegos", this.service.obtenerJuegos());
	    MV.setViewName("ABML/ABMLJuegos");
	    return MV;
	}
	
	@RequestMapping(value= "AgregarJuego2.html", params= "avanzarJuego")
	public ModelAndView GuardarJuego(String nameGame, String descripGame, String releaseDate, @RequestParam(value = "releaseCheckUS", required = false) String releaseCheckUS, 
			@RequestParam(value = "releaseCheckEU", required = false) String releaseCheckEU, @RequestParam(value = "releaseCheckJP", required = false) String releaseCheckJP, 
			Integer disGame, Integer desGame, String idGame, @RequestParam("UploadFile") MultipartFile UploadFile, HttpSession session) {
	    ModelAndView MV = new ModelAndView();
	    ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		
	    Juego juego = (Juego)context.getBean("JuegoBean");
	    
	    if(!idGame.isEmpty()) {
	    	juego = this.service.obtenerJuego(Integer.parseInt(idGame));	
	    	juego.setJuegoNombre(nameGame);
	    	juego.setJuegoDescripcion(descripGame);
	    	juego.setJuegoFechaLanzamiento(releaseDate);
	    	if(releaseCheckUS != null)
	    		juego.setRegionUS(true);
	    	else
	    		juego.setRegionUS(false);
	    	if(releaseCheckEU != null)
	    		juego.setRegionEU(true);
	    	else
	    		juego.setRegionEU(false);
	    	if(releaseCheckJP != null)
	    		juego.setRegionJP(true);
	    	else
	    		juego.setRegionJP(false);
	    	juego.setJuegoDesarrollador(this.serviceDev.obtenerDesarrollador(desGame));
	    	juego.setJuegoDistribuidor(this.serviceDis.obtenerDistribuidor(disGame));
	    	this.service.actualizarJuego(juego);
	    	String webappRoot = servletContext.getRealPath("/");
	        String relativeFolder = "/WEB-INF/assets/images/juegosTapas/";
	        String filename = webappRoot + relativeFolder + juego.getJuegoId() + ".jpg";
	    	File dirPath = new File(filename);
	    	
	    	if (!dirPath.exists()) {
	    	        dirPath.mkdirs();     
	    	}
	    	try
			{

	    		UploadFile.transferTo(dirPath);
			} catch (IllegalStateException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    	
	    	System.out.println(dirPath.getAbsoluteFile());
	    	
	    	MV.addObject("juegoAModif", juego);
	    }
	    else {
	    	juego.setJuegoNombre(nameGame);
	    	juego.setJuegoDescripcion(descripGame);
	    	juego.setJuegoFechaLanzamiento(releaseDate);
	    	juego.setJuegoPromedioResenas("0");
	    	if(releaseCheckUS != null)
	    		juego.setRegionUS(true);
	    	else
	    		juego.setRegionUS(false);
	    	if(releaseCheckEU != null)
	    		juego.setRegionEU(true);
	    	else
	    		juego.setRegionEU(false);
	    	if(releaseCheckJP != null)
	    		juego.setRegionJP(true);
	    	else
	    		juego.setRegionJP(false);
	    	juego.setJuegoDesarrollador(this.serviceDev.obtenerDesarrollador(desGame));
	    	juego.setJuegoDistribuidor(this.serviceDis.obtenerDistribuidor(disGame));
	    	juego.setJuegoId(this.service.insertarJuego(juego));
	    	String webappRoot = servletContext.getRealPath("/");
	        String relativeFolder = "/WEB-INF/assets/images/juegosTapas/";
	        String filename = webappRoot + relativeFolder + juego.getJuegoId() + ".jpg";
	    	File dirPath = new File(filename);
	    	
	    	if (!dirPath.exists()) {
	    	        dirPath.mkdirs();     
	    	}
	    	try
			{
	    		UploadFile.transferTo(dirPath);
			} catch (IllegalStateException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    	
	    	System.out.println(dirPath.getAbsoluteFile());
	    	
	    	MV.addObject("juegoAModif", juego);
	    }
	    
	    List<Genero> generos = this.serviceGen.obtenerGeneros(); 
	    
	    for(Genero gen : juego.getlGenero()) {
	    	for(int i=0; i<generos.size(); i++) {
	    		if(generos.get(i).getGeneroId() == gen.getGeneroId()) {
	    			generos.remove(i);
	    			i=0;
	    		}
	    	}
	    }
	    
	    MV.addObject("listaGeneros", generos);
	    MV.addObject("listaGenJuegos", juego.getlGenero());
	    MV.setViewName("ABML/AgregarJuego2");
	    return MV;
	}
	
	@RequestMapping(value= "AgregarGeneroJuego.html", params= "generosAgregar")
	public ModelAndView AgregarGeneroJuego(String idGame, Integer generosList) {
		ModelAndView MV = new ModelAndView();
	    ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		
	    Juego juego = (Juego)context.getBean("JuegoBean");
	    
	    if(idGame != "") {
	    	juego = this.service.obtenerJuego(Integer.parseInt(idGame));	
	    	Set<Genero> set = juego.getlGenero();
	    	set.add(serviceGen.obtenerGenero(generosList));
	    	juego.setlGenero(set);
	    	this.service.actualizarJuego(juego);
	    	MV.addObject("juegoAModif", juego);
	    	MV.setViewName("ABML/AgregarJuego2");
		    List<Genero> generos = this.serviceGen.obtenerGeneros(); 
		    
		    for(Genero gen : juego.getlGenero()) {
		    	for(int i=0; i<generos.size(); i++) {
		    		if(generos.get(i).getGeneroId() == gen.getGeneroId()) {
		    			generos.remove(i);
		    			i=0;
		    		}
		    	}
		    }
		    
		    MV.addObject("listaGeneros", generos);
		    MV.addObject("listaGenJuegos", juego.getlGenero());
	    }
	    else {
	    	MV.addObject("listaJuegos", this.service.obtenerJuegos());
	    	MV.setViewName("ABML/ABMLJuegos");
	    }

	    return MV;
	}
	
	@RequestMapping(value= "AgregarGeneroJuego.html", params= "generosEliminar")
	public ModelAndView BorrarGeneroJuego(String idGame, Integer generosJuego) {
		ModelAndView MV = new ModelAndView();
	    ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		
	    Juego juego = (Juego)context.getBean("JuegoBean");
	    
	    if(idGame != "") {
	    	juego = this.service.obtenerJuego(Integer.parseInt(idGame));	
	    	Set<Genero> set = juego.getlGenero();
	    	//set.remove(serviceGen.obtenerGenero(generosJuego));
	    	Genero genSelected = serviceGen.obtenerGenero(generosJuego);
	    	Genero genToRemove = null;
	    	
	    	for(Genero gen : set) {
		    	if(gen.getGeneroId() == genSelected.getGeneroId())
		    		genToRemove = gen;
		    }
	    	
	    	if(genToRemove != null) {
	    		set.remove(genToRemove);
	    	}
	    	
	    	juego.setlGenero(set);
	    	this.service.actualizarJuego(juego);
	    	MV.addObject("juegoAModif", juego);
	    	MV.setViewName("ABML/AgregarJuego2");
		    List<Genero> generos = this.serviceGen.obtenerGeneros(); 
		    
		    for(Genero gen : juego.getlGenero()) {
		    	for(int i=0; i<generos.size(); i++) {
		    		if(generos.get(i).getGeneroId() == gen.getGeneroId()) {
		    			generos.remove(i);
		    			i=0;
		    		}
		    	}
		    }
		    
		    MV.addObject("listaGeneros", generos);
		    MV.addObject("listaGenJuegos", juego.getlGenero());
	    }
	    else {
	    	MV.addObject("listaJuegos", this.service.obtenerJuegos());
	    	MV.setViewName("ABML/ABMLJuegos");
	    }

	    return MV;
	}
	
	@RequestMapping(value= "AgregarGeneroJuego.html", params= "volverJuego")
	public ModelAndView VolverAAgregar1(String idGame) {
		ModelAndView MV = new ModelAndView();
		
		Juego juego = this.service.obtenerJuego(Integer.parseInt(idGame));
	    if(juego != null)
	    	MV.addObject("juegoAModif", juego);
	    MV.addObject("listaDesarrolladores", this.serviceDev.obtenerDesarrolladores());
	    MV.addObject("listaDistribuidores", this.serviceDis.obtenerDistribuidores());
	    MV.addObject("listaJuegos", this.service.obtenerJuegos());
	    MV.setViewName("ABML/AgregarJuego");
		
		return MV;
	}
		
	@RequestMapping("AgregarJuego3.html")
	public ModelAndView AgregarJuego3() {
	    ModelAndView MV = new ModelAndView();
	    MV.addObject("listaJuegos", this.service.obtenerJuegos());
	    MV.setViewName("ABML/AgregarJuego3");
	    return MV;
	}
	
	@RequestMapping(value= "AgregarGeneroJuego.html", params= "avanzarJuego")
	public ModelAndView AgregarJuego3Inicio(String idGame) {
		ModelAndView MV = new ModelAndView();
	    ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		
	    Juego juego = (Juego)context.getBean("JuegoBean");
	    
	    if(idGame != "") {
	    	juego = this.service.obtenerJuego(Integer.parseInt(idGame));	
	    	MV.addObject("juegoAModif", juego);
	    	MV.setViewName("ABML/AgregarJuego3");
		    List<Plataforma> plataformas = this.servicePlat.obtenerPlataformas(); 
		    
		    for(Plataforma plat : juego.getlPlataformas()) {
		    	for(int i=0; i<plataformas.size(); i++) {
		    		if(plataformas.get(i).getPlataformaId() == plat.getPlataformaId()) {
		    			plataformas.remove(i);
		    			i=0;
		    		}
		    	}
		    }
		    
		    MV.addObject("listaPlataformas", plataformas);
		    MV.addObject("listaPlatJuegos", juego.getlPlataformas());
	    }
	    else {
	    	MV.addObject("listaJuegos", this.service.obtenerJuegos());
	    	MV.setViewName("ABML/ABMLJuegos");
	    }

	    return MV;
	}
	
	@RequestMapping(value= "AgregarPlataformaJuego.html", params= "platAgregar")
	public ModelAndView AgregarPlatJuego(String idGame, Integer platList) {
		ModelAndView MV = new ModelAndView();
	    ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		
	    Juego juego = (Juego)context.getBean("JuegoBean");
	    
	    if(idGame != "") {
	    	juego = this.service.obtenerJuego(Integer.parseInt(idGame));	
	    	Set<Plataforma> set = juego.getlPlataformas();
	    	set.add(servicePlat.obtenerPlataforma(platList));
	    	juego.setlPlataformas(set);
	    	this.service.actualizarJuego(juego);
	    	MV.addObject("juegoAModif", juego);
	    	MV.setViewName("ABML/AgregarJuego3");
		    List<Plataforma> plataformas = this.servicePlat.obtenerPlataformas(); 
		    
		    for(Plataforma plat : juego.getlPlataformas()) {
		    	for(int i=0; i<plataformas.size(); i++) {
		    		if(plataformas.get(i).getPlataformaId() == plat.getPlataformaId()) {
		    			plataformas.remove(i);
		    			i=0;
		    		}
		    	}
		    }
		    
		    MV.addObject("listaPlataformas", plataformas);
		    MV.addObject("listaPlatJuegos", juego.getlPlataformas());
	    }
	    else {
	    	MV.addObject("listaJuegos", this.service.obtenerJuegos());
	    	MV.setViewName("ABML/ABMLJuegos");
	    }

	    return MV;
	}
	
	@RequestMapping(value= "AgregarPlataformaJuego.html", params= "platEliminar")
	public ModelAndView BorrarPlatJuego(String idGame, Integer platJuego) {
		ModelAndView MV = new ModelAndView();
	    ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		
	    Juego juego = (Juego)context.getBean("JuegoBean");
	    
	    if(idGame != "") {
	    	juego = this.service.obtenerJuego(Integer.parseInt(idGame));	
	    	Set<Plataforma> set = juego.getlPlataformas();
	    	Plataforma platSelected = servicePlat.obtenerPlataforma(platJuego);
	    	Plataforma platToRemove = null;
	    	
	    	for(Plataforma plat : set) {
		    	if(plat.getPlataformaId() == platSelected.getPlataformaId())
		    		platToRemove = plat;
		    }
	    	
	    	if(platToRemove != null) {
	    		set.remove(platToRemove);
	    	}
	    	
	    	juego.setlPlataformas(set);
	    	this.service.actualizarJuego(juego);
	    	MV.addObject("juegoAModif", juego);
	    	MV.setViewName("ABML/AgregarJuego3");
		    List<Plataforma> plataformas = this.servicePlat.obtenerPlataformas(); 
		    
		    for(Plataforma plat : juego.getlPlataformas()) {
		    	for(int i=0; i<plataformas.size(); i++) {
		    		if(plataformas.get(i).getPlataformaId() == plat.getPlataformaId()) {
		    			plataformas.remove(i);
		    			i=0;
		    		}
		    	}
		    }
		    
		    MV.addObject("listaPlataformas", plataformas);
		    MV.addObject("listaPlatJuegos", juego.getlPlataformas());
	    }
	    else {
	    	MV.addObject("listaJuegos", this.service.obtenerJuegos());
	    	MV.setViewName("ABML/ABMLJuegos");
	    }

	    return MV;
	}
	
	@RequestMapping(value = "AgregarPlataformaJuego.html", params = "volverJuego")
	public ModelAndView VolverJuego(String idGame)
	{
		ModelAndView MV = new ModelAndView();

		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		
	    Juego juego = (Juego)context.getBean("JuegoBean");
	    
	    if(idGame != "") {
	    	juego = this.service.obtenerJuego(Integer.parseInt(idGame));	
	    	MV.addObject("juegoAModif", juego);
	    	MV.setViewName("ABML/AgregarJuego2");
		    List<Genero> generos = this.serviceGen.obtenerGeneros(); 
		    
		    for(Genero gen : juego.getlGenero()) {
		    	for(int i=0; i<generos.size(); i++) {
		    		if(generos.get(i).getGeneroId() == gen.getGeneroId()) {
		    			generos.remove(i);
		    			i=0;
		    		}
		    	}
		    }
		    
		    MV.addObject("listaGeneros", generos);
		    MV.addObject("listaGenJuegos", juego.getlGenero());
	    }
	    else {
	    	MV.addObject("listaJuegos", this.service.obtenerJuegos());
	    	MV.setViewName("ABML/ABMLJuegos");
	    }

		return MV;
		
	}
	
	@RequestMapping(value = "AgregarPlataformaJuego.html", params = "TerminarJuego")
	public ModelAndView TerminarJuego()
	{
		ModelAndView MV = new ModelAndView();

		MV.addObject("listaJuegos", this.service.obtenerJuegos());
		MV.setViewName("ABML/ABMLJuegos");
		return MV;

	}
	
	@RequestMapping(value ="agregarJuego.html" , method= { RequestMethod.GET, RequestMethod.POST})
	public ModelAndView agregarJuego(String juegoNombre, String juegoDescripcion, Desarrollador juegoDesarrollador, 
			Distribuidor juegoDistribuidor, String juegoFechaLanzamiento){
		ModelAndView MV = new ModelAndView();
		
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		Juego juego = (Juego)context.getBean("JuegoBean");
		
		Pais pais = new Pais();
		pais.setPaisNombre("arg");
		
		Desarrollador des = new Desarrollador();
		des.setDesarrolladorNombre("dev1");
		des.setDesarrolladorPais(pais);
		des.setDesarrolladorFechaCreacion("hoy");
		
		Distribuidor dis = new Distribuidor();
		dis.setDistribuidorNombre("dis1");
		dis.setDistribuidorPais(pais);
		dis.setDistribuidorCreacion("hoy");
		
		
		
		
		
		juego.setJuegoNombre(juegoNombre);
		juego.setJuegoDescripcion(juegoDescripcion);
		juego.setJuegoDesarrollador(juegoDesarrollador);
		juego.setJuegoDistribuidor(juegoDistribuidor); 
		juego.setJuegoFechaLanzamiento(juegoFechaLanzamiento);
		
		String Message="";
		
		try{
			
			service.insertarJuego(juego);
			Message = "Juego agregado";
		}
		catch(Exception e)
		{
			Message = "No se pudo insertar el juego";
		}
		finally
		{
		
		}
	
		MV.setViewName("Juegos");
		MV.addObject("Mensaje", Message);
		MV.addObject("listaJuegos",this.service.obtenerJuegos());
		MV.setViewName("Juegos"); 
		return MV;
		
	}
	
	//TODO metodos juego
	
}
