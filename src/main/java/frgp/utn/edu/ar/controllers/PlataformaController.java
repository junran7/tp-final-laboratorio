package frgp.utn.edu.ar.controllers;


import javax.servlet.ServletConfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;

import frgp.utn.edu.ar.dominio.Desarrollador;
import frgp.utn.edu.ar.dominio.Plataforma;
import frgp.utn.edu.ar.servicio.PlataformaServicio;

@Controller
public class PlataformaController {

	@Autowired
	public  PlataformaServicio service;
	
	@RequestMapping("ABMLPlataformas.html")
	public ModelAndView ABMLPlataformas() {
	    ModelAndView MV = new ModelAndView();
	    MV.addObject("listaPlataformas",this.service.obtenerPlataformas());
	    MV.setViewName("ABML/ABMLPlataformas");
	    return MV;
	}
	
	@RequestMapping(value="ModificarPlataforma.html", params = "eliminarPlat")
	public ModelAndView EliminarPlataforma(Integer plataformas) {
	    ModelAndView MV = new ModelAndView();
	    Plataforma plataforma = service.obtenerPlataforma(plataformas);
	    String mensaje = "Eliminado exitoso.";
	    if(plataforma != null){
	    	try {
	    		service.eliminarPlataforma(plataformas);
		    }
		    catch(Exception e) {
		    	mensaje = "La Plataforma no puede ser eliminada ya que est� en uso.";
		    }
	    }
	    MV.addObject("mensajeResultado", mensaje);
	    MV.addObject("listaPlataformas",this.service.obtenerPlataformas());
	    MV.setViewName("ABML/ABMLPlataformas");
	    return MV;
	}
	
	@RequestMapping(value="ModificarPlataforma.html", params = "modificarPlat")
	public ModelAndView ModificarPlataforma(Integer plataformas) {
	    ModelAndView MV = new ModelAndView();
	    Plataforma plataforma = service.obtenerPlataforma(plataformas);
	    if(plataforma != null)
	    	MV.addObject("plataformaAModif", plataforma);
	    MV.addObject("listaPlataformas",this.service.obtenerPlataformas());
	    MV.setViewName("ABML/ABMLPlataformas");
	    return MV;
	}
	
	@RequestMapping("AgregarPlataforma.html")
	public ModelAndView agregarPlataforma(String idPlat, String namePlat, String createdPlat) {
	    ModelAndView MV = new ModelAndView();
	    
	    ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		Plataforma plataforma = (Plataforma)context.getBean("PlataformaBean");
		
		if(idPlat != "") {
			plataforma = this.service.obtenerPlataforma(Integer.parseInt(idPlat));	
			plataforma.setPlataformaNombre(namePlat);
			plataforma.setPlataformaAnio(createdPlat);
			this.service.actualizarPlataforma(plataforma);
		}
		else {
			plataforma.setPlataformaNombre(namePlat);
			plataforma.setPlataformaAnio(createdPlat);
			this.service.insertarPlataforma(plataforma);
		}

	    MV.addObject("listaPlataformas",this.service.obtenerPlataformas());
	    MV.setViewName("ABML/ABMLPlataformas");
	    return MV;
	}
	
	@RequestMapping("/abrirPlataformas.html")
	public ModelAndView redireccion(){
		ModelAndView MV = new ModelAndView();
		MV.addObject("listaPlataformas",this.service.obtenerPlataformas());
		MV.setViewName("Plataformas"); 
		return MV;
	}
	
	
	@RequestMapping(value ="/altaPlataforma.html" , method= { RequestMethod.GET, RequestMethod.POST})
	public ModelAndView validarPlataforma(String nombreP, String anioP){
		ModelAndView MV = new ModelAndView();
		
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		Plataforma x = (Plataforma)context.getBean("PlataformaBean");	
		//tal vez si persistimos el objeto que hicimos con el bean y despues hacemos context.close() no se borra?
		x.setPlataformaNombre(nombreP);
		x.setPlataformaAnio(anioP);
		
		
		String Message="";
		
		try{
			
			service.insertarPlataforma(x);
			Message = "Plataforma agregada";
		}
		catch(Exception e)
		{
			Message = "No se pudo insertar la Plataforma";
		}
		finally
		{
		
		}
	
		MV.setViewName("Plataformas");
		MV.addObject("Mensaje", Message);
		MV.addObject("listaPlataformas",this.service.obtenerPlataformas());
		MV.setViewName("Plataformas"); 
		return MV;
		
	}
	
     
	@RequestMapping(value ="/eliminarPlataforma.html" , method= { RequestMethod.GET, RequestMethod.POST})
	public ModelAndView eliminarPlataforma(Integer id){
		ModelAndView MV = new ModelAndView();
		service.eliminarPlataforma(id);
		MV.addObject("listaPlataformas",this.service.obtenerPlataformas());
		MV.setViewName("Plataformas"); 
		MV.addObject("Mensaje", "Plataforma eliminado");
		return MV;
	}
	/*
	@RequestMapping(value = { "/delete-user-{ssoId}" }, method = RequestMethod.GET)
    public ModelAndView borrarPlataforma(@PathVariable Integer ssoId) {
		service.eliminarPlataforma(ssoId);
		ModelAndView MV = new ModelAndView();
		MV.setViewName("Plataformas");
		
		//Actualiza los generos
		MV.addObject("listaPlataformas",this.service.obtenerPlataformas());
		MV.setViewName("Plataformas"); 
		return MV;
    }*/
	
}
