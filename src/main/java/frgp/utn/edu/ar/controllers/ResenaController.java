package frgp.utn.edu.ar.controllers;

import java.nio.file.attribute.UserPrincipalLookupService;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletConfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;

import frgp.utn.edu.ar.dominio.Juego;
import frgp.utn.edu.ar.dominio.Punto;
import frgp.utn.edu.ar.dominio.Resena;
import frgp.utn.edu.ar.dominio.Usuario;
import frgp.utn.edu.ar.servicio.JuegoServicio;
import frgp.utn.edu.ar.servicio.PuntoServicio;
import frgp.utn.edu.ar.servicio.ResenaServicio;
import frgp.utn.edu.ar.servicio.UsuarioServicio;

@Controller
public class ResenaController {

	@Autowired
	public  ResenaServicio service;
	@Autowired
	public JuegoServicio serviceJuego;
	@Autowired
	public PuntoServicio servicePunto;
	@Autowired
	public UsuarioServicio serviceUser;
	
	public void init(ServletConfig config) {
		ApplicationContext ctx = WebApplicationContextUtils
				.getRequiredWebApplicationContext(config.getServletContext());
		
		this.service = (ResenaServicio) ctx.getBean("serviceResenaBean");
	}
	//TODO metodos Resena
	
	@RequestMapping(value="VotarResena.html", params="like")
	public ModelAndView likear(String idUser, String idResena, String idJuego){
		ModelAndView MV = new ModelAndView();
	
		Punto punto = new Punto();
		punto.setPuntoPositivo(true);
		punto.setPuntoResena(service.obtenerResena(Integer.parseInt(idResena)));
		punto.setPuntoUsuario(serviceUser.obtenerUsuario(Integer.parseInt(idUser)));
		
		servicePunto.insertarPunto(punto);
		Resena res = service.obtenerResena(Integer.parseInt(idResena));
		res.setResenaVotos(servicePunto.obtenerCantidadPuntos(Integer.parseInt(idResena)));
		service.actualizarResena(res);
		
		List<Resena> resenas = new ArrayList<>(service.obtenerResenas());
		Juego juego = serviceJuego.obtenerJuego(Integer.parseInt(idJuego));
		if(!idUser.isEmpty()) {
	    	Usuario user = serviceUser.obtenerUsuario(Integer.parseInt(idUser));
	    	MV.addObject("User", user);
	    }
		
		List<Punto> puntosUser = new ArrayList<>(servicePunto.obtenerPuntosUsuario(Integer.parseInt(idUser)));
		MV.addObject("UsuarioPuntos", puntosUser);
		
		MV.addObject("ListaResenas", resenas);
	    MV.addObject("JuegoAMostrar", juego);
		MV.setViewName("Usuario/Resenas");
		return MV;
	}
	
	@RequestMapping(value="VotarResena.html", params="dislike")
	public ModelAndView dislikear(String idUser, String idResena, String idJuego){
		ModelAndView MV = new ModelAndView();
	
		Punto punto = new Punto();
		punto.setPuntoPositivo(false);
		punto.setPuntoResena(service.obtenerResena(Integer.parseInt(idResena)));
		punto.setPuntoUsuario(serviceUser.obtenerUsuario(Integer.parseInt(idUser)));
		
		servicePunto.insertarPunto(punto);
		Resena res = service.obtenerResena(Integer.parseInt(idResena));
		res.setResenaVotos(servicePunto.obtenerCantidadPuntos(Integer.parseInt(idResena)));
		service.actualizarResena(res);
		
		List<Resena> resenas = new ArrayList<>(service.obtenerResenas());
		Juego juego = serviceJuego.obtenerJuego(Integer.parseInt(idJuego));
		if(!idUser.isEmpty()) {
	    	Usuario user = serviceUser.obtenerUsuario(Integer.parseInt(idUser));
	    	MV.addObject("User", user);
	    }
		
		List<Punto> puntosUser = new ArrayList<>(servicePunto.obtenerPuntosUsuario(Integer.parseInt(idUser)));
		MV.addObject("UsuarioPuntos", puntosUser);
		
		MV.addObject("ListaResenas", resenas);
	    MV.addObject("JuegoAMostrar", juego);
		MV.setViewName("Usuario/Resenas");
		return MV;
	}
	
	@RequestMapping("AgregarResena.html")  //  U N U S E D 
	public ModelAndView AgregarResena(String idResena, Integer calif, String textResena, Integer resenaJuego) { // idResena y resenaJuego van a ser hiddens
		 ModelAndView MV = new ModelAndView();
		 
		    ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		    
			Resena resena = (Resena)context.getBean("ResenaBean");
			Juego juego = serviceJuego.obtenerJuego(resenaJuego);
			
			if(idResena != "") {
				resena = this.service.obtenerResena(Integer.parseInt(idResena));	
				resena.setResenaPuntaje(calif);
				resena.setResenaDescripcion(textResena);
				resena.setResenaJuego(juego);
				
				
				juego.setJuegoPromedioResenas(service.obtenerPromedioNuevo(calif, resenaJuego));

				this.service.actualizarResena(resena);
			}
			else {
				resena.setResenaPuntaje(calif);
				resena.setResenaDescripcion(textResena);
				resena.setResenaJuego(juego);
				this.service.insertarResena(resena);
			}

		    MV.addObject("listaResenas",this.service.obtenerResenas());
		    MV.setViewName("Usuario/Resenas");
		    return MV;
	}
	
	@RequestMapping("Resenas.html")
	public ModelAndView Resenas() {
	    ModelAndView MV = new ModelAndView();
	    MV.addObject("listaResenas",this.service.obtenerResenasPorJuego(1));
	    MV.setViewName("Usuario/Resenas");
	    return MV;
	} 
}
