package frgp.utn.edu.ar.controllers;

import javax.servlet.ServletConfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;

import frgp.utn.edu.ar.dominio.Pais;
import frgp.utn.edu.ar.servicio.PaisServicio;

@Controller
public class PaisController {

	@Autowired
	public  PaisServicio service;
	
	public void init(ServletConfig config) {
		ApplicationContext ctx = WebApplicationContextUtils
				.getRequiredWebApplicationContext(config.getServletContext());
		
		this.service = (PaisServicio) ctx.getBean("servicePaisBean");
	}
	
	@RequestMapping("ABMLPaises.html")
	public ModelAndView ABMLPaises() {
	    ModelAndView MV = new ModelAndView();
	    MV.addObject("listaPaises",this.service.obtenerPaises());
	    MV.setViewName("ABML/ABMLPaises");
	    return MV;
	}
	
	@RequestMapping(value = "ModificarPais.html", params = "eliminarPais")
	public ModelAndView EliminarPais(Integer paises) {
	    ModelAndView MV = new ModelAndView();
	    Pais pais = service.obtenerPais(paises);
	    String mensaje = "Eliminado exitoso.";
	    if(pais != null) {
	    	try {
		    	service.eliminarPais(paises);
		    }
		    catch(Exception e) {
		    	mensaje = "El pa�s no puede ser eliminado ya que est� en uso.";
		    }
	    }
	    
	    MV.addObject("mensajeResultado", mensaje);
	    MV.addObject("listaPaises",this.service.obtenerPaises());
	    MV.setViewName("ABML/ABMLPaises");
	    return MV;
	}
	
	@RequestMapping(value = "ModificarPais.html", params = "modificarPais")
	public ModelAndView ModificarPais(Integer paises) {
	    ModelAndView MV = new ModelAndView();
	    Pais pais = service.obtenerPais(paises);
	    if(pais != null)
	    	MV.addObject("paisAModif", pais);
	    MV.addObject("listaPaises",this.service.obtenerPaises());
	    MV.setViewName("ABML/ABMLPaises");
	    return MV;
	}
	
	@RequestMapping(value = "AgregarPais.html")
	public ModelAndView agregarPais(String idPais, String namePais) {
	    ModelAndView MV = new ModelAndView();
	    
	    ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		Pais pais = (Pais)context.getBean("PaisBean");
		
		if(idPais != "") {
			pais = this.service.obtenerPais(Integer.parseInt(idPais));	
			pais.setPaisNombre(namePais);
			this.service.actualizarPais(pais);
		}
		else {
			pais.setPaisNombre(namePais);
			this.service.insertarPais(pais);
		}

	    MV.addObject("listaPaises",this.service.obtenerPaises());
	    MV.setViewName("ABML/ABMLPaises");
	    return MV;
	}
	
}
