package frgp.utn.edu.ar.dao;

import java.util.ArrayList;

import frgp.utn.edu.ar.dominio.Punto;
import frgp.utn.edu.ar.dominio.Resena;

public interface PuntoDao {

	public void insertarPunto(Punto Punto);

	public ArrayList<Punto> obtenerPuntos();

	public void actualizarPunto(Punto Punto);

	public int obtenerCantidadPuntos(Integer idResena);

	public ArrayList<Punto> obtenerPunto(Integer idResena, Integer idUsuario);

	public void eliminarPunto(Punto punto);

	ArrayList<Punto> obtenerPuntosUsuario(Integer idUsuario);
	
}
