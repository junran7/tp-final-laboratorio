package frgp.utn.edu.ar.dao;

import java.util.ArrayList;

import frgp.utn.edu.ar.dominio.Genero;

public interface GeneroDao {

	//Alta de persona
		public void insertarGenero(Genero genero);

		//Obtiene todas las presonas
		public ArrayList<Genero> obtenerGeneros();

		//Elimina una presona a aprtir del dni
		public void eliminarGenero(Integer idGenero);

		//Actualiza los datos de una persona
		public void actualizarGenero(Genero genero);

		Genero obtenerGenero(Integer id);
}
