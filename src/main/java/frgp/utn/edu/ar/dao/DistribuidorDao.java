package frgp.utn.edu.ar.dao;

import java.util.ArrayList;

import frgp.utn.edu.ar.dominio.Distribuidor;

public interface DistribuidorDao {

	public void insertarDistribuidor(Distribuidor distribuidor);

	public ArrayList<Distribuidor> obtenerDistribuidores();

	public void eliminarDistribuidor(Integer idDistribuidor);

	public void actualizarDistribuidor(Distribuidor distribuidor);
	
	public Distribuidor obtenerDistribuidor(Integer idDistribuidor);
	
}
