package frgp.utn.edu.ar.dao;

import java.util.ArrayList;

import frgp.utn.edu.ar.dominio.Juego;

public interface JuegoDao {

			public Integer insertarJuego(Juego juego);

			public ArrayList<Juego> obtenerJuegos();

			public void eliminarJuego(Integer idJuego);

			public void actualizarJuego(Juego juego);

			Juego obtenerJuego(Integer idJuego);

			ArrayList<Juego> buscarJuegosPorNombre(String nombreJuego);

			ArrayList<Juego> queryJuego(String query);
	
}
