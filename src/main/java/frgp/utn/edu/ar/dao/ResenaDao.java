package frgp.utn.edu.ar.dao;

import java.util.ArrayList;

import frgp.utn.edu.ar.dominio.Resena;

public interface ResenaDao {

	public void insertarResena(Resena Resena);

	public ArrayList<Resena> obtenerResenas();

	public void eliminarResena(Integer idResena);

	public void actualizarResena(Resena Resena);

	Double obtenerCantidadResenas(Integer idJuego);

	Double obtenerSumaPuntaje(Integer idJuego);

	Resena obtenerResena(Integer resenaId);

	ArrayList<Resena> obtenerResenasPorJuego(Integer idJuego);

	ArrayList<Resena> obtenerResenasPorJuegoYUsuario(Integer idJuego, Integer idUsuario);
	
}
