package frgp.utn.edu.ar.servicioImpl;

import java.util.ArrayList;

import frgp.utn.edu.ar.dao.JuegoDao;
import frgp.utn.edu.ar.dominio.Juego;
import frgp.utn.edu.ar.servicio.JuegoServicio;


public class JuegoServicioImpl implements JuegoServicio{
private JuegoDao dataAccess = null;
	
	public void setDataAccess(JuegoDao dataAccess) {
		this.dataAccess = dataAccess;
	}
	
	@Override
	public ArrayList<Juego> obtenerJuegos() {

		return dataAccess.obtenerJuegos();
	}
	
	@Override
	public Juego obtenerJuego(Integer idJuego) {

		return dataAccess.obtenerJuego(idJuego);
	}

	@Override
	public Integer insertarJuego(Juego juego) {

		return dataAccess.insertarJuego(juego);
		
	}

	@Override
	public void eliminarJuego(Integer idJuego) {

		dataAccess.eliminarJuego(idJuego);
		
	}

	@Override
	public void actualizarJuego(Juego juego) {

		dataAccess.actualizarJuego(juego);
		
	}
	
	@Override
	public ArrayList<Juego> obtenerJuegosPorNombre(String juegoNombre)
	{
		return dataAccess.buscarJuegosPorNombre(juegoNombre);
	}

	@Override
	public ArrayList<Juego> queryJuego(String query) {
		return dataAccess.queryJuego(query);
	}
}
