package frgp.utn.edu.ar.servicioImpl;

import java.text.DecimalFormat;
import java.util.ArrayList;

import frgp.utn.edu.ar.dao.ResenaDao;
import frgp.utn.edu.ar.dominio.Resena;
import frgp.utn.edu.ar.servicio.ResenaServicio;

public class ResenaServicioImpl implements ResenaServicio{

private ResenaDao dataAccess = null;
	
	public void setDataAccess(ResenaDao dataAccess) {
		this.dataAccess = dataAccess;
	}
	
	@Override
	public ArrayList<Resena> obtenerResenas() {

		return dataAccess.obtenerResenas();
	}

	@Override
	public ArrayList<Resena> obtenerResenasPorJuego(Integer idJuego){
		return dataAccess.obtenerResenasPorJuego(idJuego);
	}

	
	@Override
	public void insertarResena(Resena resena) {

		dataAccess.insertarResena(resena);
		
	}

	@Override
	public void eliminarResena(Integer idResena) {

		dataAccess.eliminarResena(idResena);
		
	}

	@Override
	public void actualizarResena(Resena resena) {

		dataAccess.actualizarResena(resena);
		
	}
	
	@Override
	public Resena obtenerResena(Integer resenaId) {
		
		
		
		return dataAccess.obtenerResena(resenaId);
	}
	
	@Override
	public String obtenerPromedioNuevo(Integer puntajeJuego, Integer idJuego) {
		//DecimalFormat df2 = new DecimalFormat("#.##");
		Double puntajeNuevo = dataAccess.obtenerSumaPuntaje(idJuego) + puntajeJuego;
		Double cantidadNueva = dataAccess.obtenerCantidadResenas(idJuego) + 1;

		double promedioNuevo = (double)(puntajeNuevo / cantidadNueva);
		//return df2.format(promedioNuevo);
		Math.round(promedioNuevo);
		return Double.toString(promedioNuevo);
	}
	
	@Override
	public ArrayList<Resena> obtenerResenasPorJuegoYUsuario(Integer idJuego, Integer idUsuario) {
		return dataAccess.obtenerResenasPorJuegoYUsuario(idJuego, idUsuario);
	}
}
