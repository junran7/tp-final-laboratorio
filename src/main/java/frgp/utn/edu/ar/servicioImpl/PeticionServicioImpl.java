package frgp.utn.edu.ar.servicioImpl;

import java.util.ArrayList;

import frgp.utn.edu.ar.dao.PeticionDao;
import frgp.utn.edu.ar.dominio.Peticion;
import frgp.utn.edu.ar.servicio.PeticionServicio;

public class PeticionServicioImpl implements PeticionServicio{

private PeticionDao dataAccess = null;
	
	public void setDataAccess(PeticionDao dataAccess) {
		this.dataAccess = dataAccess;
	}
	
	@Override
	public ArrayList<Peticion> obtenerPeticiones() {

		return dataAccess.obtenerPeticiones();
	}
	
	@Override
	public Peticion obtenerPeticion(Integer id) {

		return dataAccess.obtenerPeticion(id);
	}

	@Override
	public void insertarPeticion(Peticion peticion) {

		dataAccess.insertarPeticion(peticion);
		
	}

	@Override
	public void eliminarPeticion(Integer idPeticion) {

		dataAccess.eliminarPeticion(idPeticion);
		
	}

	@Override
	public void actualizarPeticion(Peticion peticion) {

		dataAccess.actualizarPeticion(peticion);
		
	}
	
}
