package frgp.utn.edu.ar.servicioImpl;

import java.util.ArrayList;

import frgp.utn.edu.ar.dao.DesarrolladorDao;
import frgp.utn.edu.ar.dominio.Desarrollador;
import frgp.utn.edu.ar.servicio.DesarrolladorServicio;

public class DesarrolladorServicioImpl implements DesarrolladorServicio{

	private DesarrolladorDao dataAccess = null;
	
	public void setDataAccess(DesarrolladorDao dataAccess) {
		this.dataAccess = dataAccess;
	}
	
	@Override
	public ArrayList<Desarrollador> obtenerDesarrolladores() {

		return dataAccess.obtenerDesarrolladores();
	}

	@Override
	public void insertarDesarrollador(Desarrollador desarrollador) {

		dataAccess.insertarDesarrollador(desarrollador);
		
	}

	@Override
	public void eliminarDesarrollador(Integer idDesarrollador) {

		dataAccess.eliminarDesarrollador(idDesarrollador);
		
	}

	@Override
	public void actualizarDesarrollador(Desarrollador desarrollador) {

		dataAccess.actualizarDesarrollador(desarrollador);
		
	}
	
	@Override
	public Desarrollador obtenerDesarrollador(Integer idDesarrollador) {

		return dataAccess.obtenerDesarrollador(idDesarrollador);
	}
	
	
	
}
