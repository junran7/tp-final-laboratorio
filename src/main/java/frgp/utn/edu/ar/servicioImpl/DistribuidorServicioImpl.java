package frgp.utn.edu.ar.servicioImpl;

import java.util.ArrayList;

import frgp.utn.edu.ar.dao.DistribuidorDao;
import frgp.utn.edu.ar.dominio.Distribuidor;
import frgp.utn.edu.ar.servicio.DistribuidorServicio;

public class DistribuidorServicioImpl implements DistribuidorServicio{
	
private DistribuidorDao dataAccess = null;
	
	public void setDataAccess(DistribuidorDao dataAccess) {
		this.dataAccess = dataAccess;
	}
	
	@Override
	public ArrayList<Distribuidor> obtenerDistribuidores() {

		return dataAccess.obtenerDistribuidores();
	}

	@Override
	public void insertarDistribuidor(Distribuidor distribuidor) {

		dataAccess.insertarDistribuidor(distribuidor);
		
	}

	@Override
	public void eliminarDistribuidor(Integer idDistribuidor) {

		dataAccess.eliminarDistribuidor(idDistribuidor);
		
	}

	@Override
	public void actualizarDistribuidor(Distribuidor distribuidor) {

		dataAccess.actualizarDistribuidor(distribuidor);
		
	}
	
	@Override
	public Distribuidor obtenerDistribuidor(Integer idDistribuidor) {

		return dataAccess.obtenerDistribuidor(idDistribuidor);
	}

}
