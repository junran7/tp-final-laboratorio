package frgp.utn.edu.ar.servicioImpl;

import java.util.ArrayList;

import frgp.utn.edu.ar.dao.GeneroDao;
import frgp.utn.edu.ar.dominio.Genero;
import frgp.utn.edu.ar.servicio.GeneroServicio;

public class GeneroServicioImpl implements GeneroServicio {

	private GeneroDao dataAccess = null;
	
	public void setDataAccess(GeneroDao dataAccess) {
		this.dataAccess = dataAccess;
	}
	
	@Override
	public ArrayList<Genero> obtenerGeneros() {

		return dataAccess.obtenerGeneros();
	}
	
	@Override
	public Genero obtenerGenero(Integer id) {

		return dataAccess.obtenerGenero(id);
	}

	@Override
	public void insertarGenero(Genero genero) {

		dataAccess.insertarGenero(genero);
		
	}

	@Override
	public void eliminarGenero(Integer idGenero) {

		dataAccess.eliminarGenero(idGenero);
		
	}

	@Override
	public void actualizarGenero(Genero genero) {

		dataAccess.actualizarGenero(genero);
		
	}

}
