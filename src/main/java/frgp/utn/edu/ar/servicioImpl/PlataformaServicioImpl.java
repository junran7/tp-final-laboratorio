package frgp.utn.edu.ar.servicioImpl;

import java.util.ArrayList;

import frgp.utn.edu.ar.dao.PlataformaDao;
import frgp.utn.edu.ar.dominio.Plataforma;
import frgp.utn.edu.ar.servicio.PlataformaServicio;

public class PlataformaServicioImpl implements PlataformaServicio{

private PlataformaDao dataAccess = null;
	
	public void setDataAccess(PlataformaDao dataAccess) {
		this.dataAccess = dataAccess;
	}
	
	@Override
	public ArrayList<Plataforma> obtenerPlataformas() {

		return dataAccess.obtenerPlataformas();
	}

	@Override
	public void insertarPlataforma(Plataforma plataforma) {

		dataAccess.insertarPlataforma(plataforma);
		
	}

	@Override
	public void eliminarPlataforma(Integer idPlataforma) {

		dataAccess.eliminarPlataforma(idPlataforma);
		
	}

	@Override
	public void actualizarPlataforma(Plataforma plataforma) {

		dataAccess.actualizarPlataforma(plataforma);
	}
	
	@Override
	public Plataforma obtenerPlataforma(Integer idPlataforma) {

		return dataAccess.obtenerPlataforma(idPlataforma);
	}
}
