package frgp.utn.edu.ar.servicioImpl;

import java.util.ArrayList;

import frgp.utn.edu.ar.dao.UsuarioDao;
import frgp.utn.edu.ar.dominio.Usuario;
import frgp.utn.edu.ar.servicio.UsuarioServicio;

public class UsuarioServicioImpl implements UsuarioServicio
{

	private UsuarioDao dataAccess = null;

	public void setDataAccess(UsuarioDao dataAccess)
	{
		this.dataAccess = dataAccess;
	}

	@Override
	public ArrayList<Usuario> obtenerUsuarios()
	{
		return dataAccess.obtenerUsuarios();
	}

	@Override
	public Usuario obtenerUnRegistro(String nombreUser)
	{
		return dataAccess.obtenerUsuarioPorNombre(nombreUser);
	}

	@Override
	public void insertarUsuario(Usuario usuario)
	{
		dataAccess.insertarUsuario(usuario);

	}

	@Override
	public void eliminarUsuario(Integer idUser)
	{
		dataAccess.eliminarUsuario(idUser);

	}

	@Override
	public void actualizarUsuario(Usuario usuario)
	{
		dataAccess.actualizarUsuario(usuario);

	}

	public String registrarUsuario(Usuario usuario)
	{
		try
		{
			insertarUsuario(usuario);
			return "Se ha Registrado con �xito!";
		} catch (Exception e)
		{
			return "Ha ocurrido un error.";
		}
	}

	@Override
	public boolean existeEmail(String email)
	{
		return dataAccess.existeEmail(email);
	}

	@Override
	public Usuario obtenerUsuarioLogin(String email, String pass)
	{
		return dataAccess.obtenerUsuarioLogin(email, pass);
	}

	@Override
	public boolean existeEmail(String email, int id)
	{
		return dataAccess.existeEmail(email, id);
	}

	@Override
	public boolean contrasenaValida(int id, String pass)
	{
		return dataAccess.contrasenaValida(id, pass);
	}

	@Override
	public void banearUsuario(Usuario user)
	{
		user.setUsuarioBaneado(true);
		actualizarUsuario(user);
	}

	@Override
	public void desbanearUsuario(Usuario user)
	{
		user.setUsuarioBaneado(false);
		actualizarUsuario(user);
	}

	@Override
	public void adminearUsuario(Usuario user)
	{
		user.setUsuarioAdmin(true);
		actualizarUsuario(user);
	}

	@Override
	public Usuario obtenerUsuario(int id)
	{
		return dataAccess.obtenerUsuario(id);
	}
}
