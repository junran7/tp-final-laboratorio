package frgp.utn.edu.ar.dominio;

import javax.persistence.*;

@Entity
@Table(name = "Desarrolladores")
public class Desarrollador {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int DesarrolladorId;
	private String DesarrolladorNombre;
	private String DesarrolladorFechaCreacion;
	@ManyToOne(fetch= FetchType.EAGER)
	@JoinColumn(name="PaisId")
	private Pais DesarrolladorPais;
	
	//manytoone paisId
	//private Pais DesarrolladorPais;
	
	@Override
	public String toString() {
		return "Desarrollador [DesarrolladorId=" + DesarrolladorId + ", DesarrolladorNombre=" + DesarrolladorNombre
				+ ", DesarrolladorFechaCreacion=" + DesarrolladorFechaCreacion + ", DesarrolladorPais="
				+ DesarrolladorPais + "]";
	}

	public int getDesarrolladorId() {
		return DesarrolladorId;
	}

	public void setDesarrolladorId(int desarrolladorId) {
		DesarrolladorId = desarrolladorId;
	}

	public String getDesarrolladorNombre() {
		return DesarrolladorNombre;
	}

	public void setDesarrolladorNombre(String desarrolladorNombre) {
		DesarrolladorNombre = desarrolladorNombre;
	}

	public String getDesarrolladorFechaCreacion() {
		return DesarrolladorFechaCreacion;
	}

	public void setDesarrolladorFechaCreacion(String desarrolladorFechaCreacion) {
		DesarrolladorFechaCreacion = desarrolladorFechaCreacion;
	}

	public Pais getDesarrolladorPais() {
		return DesarrolladorPais;
	}

	public void setDesarrolladorPais(Pais desarrolladorPais) {
		DesarrolladorPais = desarrolladorPais;
	}

	
	
}
