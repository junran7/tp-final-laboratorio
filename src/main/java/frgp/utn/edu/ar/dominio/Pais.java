package frgp.utn.edu.ar.dominio;

import javax.persistence.*;

@Entity
@Table (name = "Paises")
public class Pais {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int PaisId;
	private String PaisNombre;
	
	
	@Override
	public String toString() {
		return "Pais [PaisId=" + PaisId + ", PaisNombre=" + PaisNombre + "]";
	}


	public int getPaisId() {
		return PaisId;
	}


	public void setPaisId(int paisId) {
		PaisId = paisId;
	}


	public String getPaisNombre() {
		return PaisNombre;
	}


	public void setPaisNombre(String paisNombre) {
		PaisNombre = paisNombre;
	}
	
	
	
	
	
}
