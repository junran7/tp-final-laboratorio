package frgp.utn.edu.ar.dominio;

import javax.persistence.*;

@Entity
public class Staff {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int StaffId;
	private String StaffNombre;
	private String StaffApellido;
	private String StaffNacimiento;
	private boolean StaffSexo;
	private String StaffNacionalidad; //cambiar a pais tal vez?
	public int getStaffId() {
		return StaffId;
	}
	public void setStaffId(int staffId) {
		StaffId = staffId;
	}
	public String getStaffNombre() {
		return StaffNombre;
	}
	public void setStaffNombre(String staffNombre) {
		StaffNombre = staffNombre;
	}
	public String getStaffApellido() {
		return StaffApellido;
	}
	public void setStaffApellido(String staffApellido) {
		StaffApellido = staffApellido;
	}
	public String getStaffNacimiento() {
		return StaffNacimiento;
	}
	public void setStaffNacimiento(String staffNacimiento) {
		StaffNacimiento = staffNacimiento;
	}
	public boolean isStaffSexo() {
		return StaffSexo;
	}
	public void setStaffSexo(boolean staffSexo) {
		StaffSexo = staffSexo;
	}
	public String getStaffNacionalidad() {
		return StaffNacionalidad;
	}
	public void setStaffNacionalidad(String staffNacionalidad) {
		StaffNacionalidad = staffNacionalidad;
	}
	
	
	
}
