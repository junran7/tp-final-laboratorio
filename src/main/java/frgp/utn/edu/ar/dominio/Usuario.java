package frgp.utn.edu.ar.dominio;

import javax.persistence.*;

@Entity
@Table(name = "Usuarios")
public class Usuario
{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="UsuarioId")
	private Integer UsuarioId;
	private String UsuarioNombre;
	private String UsuarioContrasena;
	private String UsuarioEmail;
	private String UsuarioFechacreacion;
	private String UsuarioBio;
	private Boolean UsuarioAdmin;
	private Boolean UsuarioBorrado;
	private Boolean UsuarioBaneado;
	
	
	
	public Usuario(String usuarioNombre, String usuarioContrasena, String usuarioEmail, String usuarioFechacreacion,
			String usuarioBio, Boolean usuarioAdmin, Boolean usuarioBorrado, Boolean usuarioBaneado) {
		super();
		UsuarioNombre = usuarioNombre;
		UsuarioContrasena = usuarioContrasena;
		UsuarioEmail = usuarioEmail;
		UsuarioFechacreacion = usuarioFechacreacion;
		UsuarioBio = usuarioBio;
		UsuarioAdmin = usuarioAdmin;
		UsuarioBorrado = usuarioBorrado;
		UsuarioBaneado = usuarioBaneado;
	}



	@Override
	public String toString() {
		return "Usuario [UsuarioId=" + UsuarioId + ", UsuarioNombre=" + UsuarioNombre + ", UsuarioContrasena="
				+ UsuarioContrasena + ", UsuarioEmail=" + UsuarioEmail + ", UsuarioFechacreacion="
				+ UsuarioFechacreacion + ", UsuarioBio=" + UsuarioBio + ", UsuarioAdmin=" + UsuarioAdmin
				+ ", UsuarioBorrado=" + UsuarioBorrado + ", UsuarioBaneado=" + UsuarioBaneado + "]";
	}



	public int getUsuarioId() {
		return UsuarioId;
	}



	public void setUsuarioId(int usuarioId) {
		UsuarioId = usuarioId;
	}



	public String getUsuarioNombre() {
		return UsuarioNombre;
	}



	public void setUsuarioNombre(String usuarioNombre) {
		UsuarioNombre = usuarioNombre;
	}



	public String getUsuarioContrasena() {
		return UsuarioContrasena;
	}



	public void setUsuarioContrasena(String usuarioContrasena) {
		UsuarioContrasena = usuarioContrasena;
	}



	public String getUsuarioEmail() {
		return UsuarioEmail;
	}



	public void setUsuarioEmail(String usuarioEmail) {
		UsuarioEmail = usuarioEmail;
	}



	public String getUsuarioFechacreacion() {
		return UsuarioFechacreacion;
	}



	public void setUsuarioFechacreacion(String usuarioFechacreacion) {
		UsuarioFechacreacion = usuarioFechacreacion;
	}



	public String getUsuarioBio() {
		return UsuarioBio;
	}



	public void setUsuarioBio(String usuarioBio) {
		UsuarioBio = usuarioBio;
	}



	public Boolean getUsuarioAdmin() {
		return UsuarioAdmin;
	}



	public void setUsuarioAdmin(Boolean usuarioAdmin) {
		UsuarioAdmin = usuarioAdmin;
	}



	public Boolean getUsuarioBorrado() {
		return UsuarioBorrado;
	}



	public void setUsuarioBorrado(Boolean usuarioBorrado) {
		UsuarioBorrado = usuarioBorrado;
	}



	public Boolean getUsuarioBaneado() {
		return UsuarioBaneado;
	}



	public void setUsuarioBaneado(Boolean usuarioBaneado) {
		UsuarioBaneado = usuarioBaneado;
	}



	public Usuario()
	{}

}