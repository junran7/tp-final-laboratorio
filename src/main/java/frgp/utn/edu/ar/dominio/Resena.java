package frgp.utn.edu.ar.dominio;

import javax.persistence.*;

@Entity
@Table (name = "Resenas")
public class Resena {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int ResenaId;
	private int ResenaPuntaje;
	private String ResenaDescripcion;
	private int ResenaVotos;
	@ManyToOne(fetch= FetchType.EAGER)
	@JoinColumn(name="UsuarioId")
	private Usuario ResenaUsuario;
	@ManyToOne(fetch= FetchType.EAGER)
	@JoinColumn(name="JuegoId")
	private Juego ResenaJuego;
	private String ResenaFecha;
	
	@Override
	public String toString() {
		return "Resenas [ResenaId=" + ResenaId + ", ResenaPuntaje=" + ResenaPuntaje + ", ResenaDescripcion="
				+ ResenaDescripcion + ", ResenaVotos=" + ResenaVotos + ", ResenaUsuario=" + ResenaUsuario
				+ ", ResenaJuego=" + ResenaJuego + ", ResenaFecha=" + ResenaFecha + "]";
	}


	public int getResenaId() {
		return ResenaId;
	}


	public void setResenaId(int resenaId) {
		ResenaId = resenaId;
	}


	public int getResenaPuntaje() {
		return ResenaPuntaje;
	}


	public void setResenaPuntaje(int resenaPuntaje) {
		ResenaPuntaje = resenaPuntaje;
	}


	public String getResenaDescripcion() {
		return ResenaDescripcion;
	}


	public void setResenaDescripcion(String resenaDescripcion) {
		ResenaDescripcion = resenaDescripcion;
	}


	public int getResenaVotos() {
		return ResenaVotos;
	}


	public void setResenaVotos(int resenaVotos) {
		ResenaVotos = resenaVotos;
	}


	public Usuario getResenaUsuario() {
		return ResenaUsuario;
	}


	public void setResenaUsuario(Usuario resenaUsuario) {
		ResenaUsuario = resenaUsuario;
	}


	public Juego getResenaJuego() {
		return ResenaJuego;
	}


	public void setResenaJuego(Juego resenaJuego) {
		ResenaJuego = resenaJuego;
	}


	public String getResenaFecha() {
		return ResenaFecha;
	}


	public void setResenaFecha(String resenaFecha) {
		ResenaFecha = resenaFecha;
	}
	
	
}
