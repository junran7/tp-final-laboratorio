package frgp.utn.edu.ar.dominio;

import javax.persistence.*;

@Entity
public class Plataforma {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int PlataformaId;
	private String PlataformaNombre;
	private String PlataformaAnio;
	
	@Override
	public String toString() {
		return "Plataforma [PlataformaId=" + PlataformaId + ", PlataformaNombre=" + PlataformaNombre
				+ ", PlataformaAnio=" + PlataformaAnio + "]";
	}

	public int getPlataformaId() {
		return PlataformaId;
	}

	public void setPlataformaId(int plataformaId) {
		PlataformaId = plataformaId;
	}

	public String getPlataformaNombre() {
		return PlataformaNombre;
	}

	public void setPlataformaNombre(String plataformaNombre) {
		PlataformaNombre = plataformaNombre;
	}

	public String getPlataformaAnio() {
		return PlataformaAnio;
	}

	public void setPlataformaAnio(String plataformaAnio) {
		PlataformaAnio = plataformaAnio;
	}
	
	
	
	
}
