package frgp.utn.edu.ar.dominio;

import java.io.Serializable;

import javax.persistence.*;

@SuppressWarnings("serial")
@Entity
@Table (name = "Puntos")
public class Punto implements Serializable{

	@Id
	@ManyToOne(fetch= FetchType.EAGER)
	@JoinColumn(name="UsuarioId")
	private Usuario PuntoUsuario;
	@Id
	@ManyToOne(fetch= FetchType.EAGER)
	@JoinColumn(name="ResenaId")
	private Resena PuntoResena;
	
	private Boolean PuntoPositivo = true;
	
	@Override
	public String toString() {
		return "Punto [PuntoUsuario=" + PuntoUsuario + ", PuntoResena=" + PuntoResena + ", PuntoPositivo="
				+ PuntoPositivo + "]";
	}

	public Usuario getPuntoUsuario() {
		return PuntoUsuario;
	}

	public void setPuntoUsuario(Usuario puntoUsuario) {
		PuntoUsuario = puntoUsuario;
	}

	public Resena getPuntoResena() {
		return PuntoResena;
	}

	public void setPuntoResena(Resena puntoResena) {
		PuntoResena = puntoResena;
	}

	public Boolean getPuntoPositivo() {
		return PuntoPositivo;
	}

	public void setPuntoPositivo(Boolean puntoPositivo) {
		PuntoPositivo = puntoPositivo;
	}

	

	
	
	
	
}
