package frgp.utn.edu.ar.dominio;

import javax.persistence.*;

@Entity
public class Peticion {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int PeticionId;
	@ManyToOne
	@JoinColumn(name="UsuarioId")
	private Usuario PeticionUsuario;
	private String PeticionJuegoNombre;
	private String PeticionDetalle;
	private boolean PeticionCumplida;
	
	@Override
	public String toString() {
		return "Peticion [PeticionId=" + PeticionId + ", PeticionUsuario=" + PeticionUsuario + ", PeticionJuegoNombre="
				+ PeticionJuegoNombre + ", PeticionDetalle=" + PeticionDetalle + ", PeticionCumplida="
				+ PeticionCumplida + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()="
				+ super.toString() + "]";
	}

	public int getPeticionId() {
		return PeticionId;
	}

	public void setPeticionId(int peticionId) {
		PeticionId = peticionId;
	}

	public Usuario getPeticionUsuario() {
		return PeticionUsuario;
	}

	public void setPeticionUsuario(Usuario peticionUsuario) {
		PeticionUsuario = peticionUsuario;
	}

	public String getPeticionJuegoNombre() {
		return PeticionJuegoNombre;
	}

	public void setPeticionJuegoNombre(String peticionJuegoNombre) {
		PeticionJuegoNombre = peticionJuegoNombre;
	}

	public String getPeticionDetalle() {
		return PeticionDetalle;
	}

	public void setPeticionDetalle(String peticionDetalle) {
		PeticionDetalle = peticionDetalle;
	}

	public boolean isPeticionCumplida() {
		return PeticionCumplida;
	}

	public void setPeticionCumplida(boolean peticionCumplida) {
		PeticionCumplida = peticionCumplida;
	}
	
	
	
	
}
