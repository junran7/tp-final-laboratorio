package frgp.utn.edu.ar.dominio;

import javax.servlet.ServletConfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import frgp.utn.edu.ar.servicio.JuegoServicio;

public class TEST {
	
	@Autowired
	public static  JuegoServicio service;
	
	public void init(ServletConfig config) {
		ApplicationContext ctx = WebApplicationContextUtils
				.getRequiredWebApplicationContext(config.getServletContext());
		
		TEST.service = (JuegoServicio) ctx.getBean("serviceJuegoBean");
	}

	public static void main(String[] args) {

		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		Juego juego = (Juego)context.getBean("JuegoBean");
		
		Pais pais = new Pais();
		pais.setPaisNombre("arg");
		
		Desarrollador des = new Desarrollador();
		des.setDesarrolladorNombre("dev1");
		des.setDesarrolladorPais(pais);
		des.setDesarrolladorFechaCreacion("hoy");
		
		Distribuidor dis = new Distribuidor();
		dis.setDistribuidorNombre("dis1");
		dis.setDistribuidorPais(pais);
		dis.setDistribuidorCreacion("hoy");
		
		
		
		
		
		juego.setJuegoNombre("asd");
		juego.setJuegoDescripcion("asd");
		juego.setJuegoDesarrollador(des);
		juego.setJuegoDistribuidor(dis); 
		juego.setJuegoFechaLanzamiento("ayer");
		
		String Message="";
		
		try{
			
			service.insertarJuego(juego);
			Message = "Juego agregado";
			System.out.println(juego.toString());
		}
		catch(Exception e)
		{
			Message = "No se pudo insertar el juego";
			System.out.println(e.toString());
		}
		finally
		{
			System.out.println(Message);
		}
		
	}

}
