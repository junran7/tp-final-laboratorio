package frgp.utn.edu.ar.dominio;

import javax.persistence.*;

@Entity
@Table (name = "Distribuidores")
public class Distribuidor {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int DistribuidorId;
	private String DistribuidorNombre;
	private String DistribuidorCreacion;
	@ManyToOne(fetch= FetchType.EAGER)
	@JoinColumn(name="PaisId")
	private Pais DistribuidorPais;
	
	public int getDistribuidorId() {
		return DistribuidorId;
	}
	public void setDistribuidorId(int distribuidorId) {
		DistribuidorId = distribuidorId;
	}
	public String getDistribuidorNombre() {
		return DistribuidorNombre;
	}
	public void setDistribuidorNombre(String distribuidorNombre) {
		DistribuidorNombre = distribuidorNombre;
	}
	public String getDistribuidorCreacion() {
		return DistribuidorCreacion;
	}
	public void setDistribuidorCreacion(String distribuidorCreacion) {
		DistribuidorCreacion = distribuidorCreacion;
	}
	public Pais getDistribuidorPais() {
		return DistribuidorPais;
	}
	public void setDistribuidorPais(Pais distribuidorPais) {
		DistribuidorPais = distribuidorPais;
	}
	
	
	
}
