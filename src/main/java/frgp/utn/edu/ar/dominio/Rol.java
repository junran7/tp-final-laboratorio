package frgp.utn.edu.ar.dominio;

import javax.persistence.*;

@Entity
@Table(name = "Roles")
public class Rol {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int RolId;
	private String RolNombre;
	
	public int getRolId() {
		return RolId;
	}
	public void setRolId(int rolId) {
		RolId = rolId;
	}
	public String getRolNombre() {
		return RolNombre;
	}
	public void setRolNombre(String rolNombre) {
		RolNombre = rolNombre;
	}
	
	
}
